﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model.Database;
using Model.Database.Entity;
using System.Collections.Generic;

namespace TestChatMessenger
{
    /// <summary>
    /// This test class verify operations to <see cref="DBUserManager"/> class.
    /// </summary>
    /// <remarks>
    /// The database used is a test database and is created with the script `test_database.sql`.
    /// Make sure to run the sql script before each session, i.e. if there is a insert query and a unique constraint.
    /// </remarks>
    [TestClass]
    public class TestDBUserManager
    {
        private const string connectionString = "SERVER=localhost;" +
                                                "DATABASE=test_ybznrn;" +
                                                "UID=test_ybznrn_tester;" +
                                                "PASSWORD=Pa$$w0rd";

        private DBUserManager userManager;
        private User user;

        /// <summary>
        /// This test method is designed to test when a user log in to the database.
        /// </summary>
        [TestMethod]
        public void Login_RightInformation_Success()
        {
            //given
            user = new User("test@cpnv.ch", "qwe123!$");
            userManager = new DBUserManager(connectionString, user);
            int expectedId = 1;

            //when
            userManager.Login();

            //then
            Assert.IsTrue(user.IsConnected);
            Assert.AreEqual(expectedId, user.Id);
        }

        /// <summary>
        /// This test method is designed to test when a user log in to the database but his email is not found.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(DBUserManager.LoginFailException))]
        public void Login_EmailNotFound_Exception()
        {
            //given
            user = new User("test1234@cpnv.ch", "qwe123!$");
            userManager = new DBUserManager(connectionString, user);

            //when
            userManager.Login();
        }

        /// <summary>
        /// This test method is designed to test when a user log in to the database but his password doesn't match.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(DBUserManager.LoginFailException))]
        public void Login_PasswordNotMatch_Exception()
        {
            //given
            user = new User("test@cpnv.ch", "notthepassword");
            userManager = new DBUserManager(connectionString, user);

            //when
            userManager.Login();
        }

        /// <summary>
        /// This test method is designed to test when a user try to register and log in with his new account.
        /// </summary>
        [TestMethod]
        public void Register_LoginAfter_Success()
        {
            //given
            user = new User("newUser@cpnv.ch", "Str0ng.Pa$$w0rd");
            userManager = new DBUserManager(connectionString, user);

            //when
            userManager.Register();
            userManager.Login();

            //then
            Assert.IsTrue(user.IsConnected);
        }

        /// <summary>
        /// This test method is designed to test when a user try to register with an email that already exists in the database.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(DBUserManager.UserAlreadyExistsException))]
        public void Register_UserAlreadyExists_Exception()
        {
            //given
            user = new User("test@cpnv.ch", "Str0ng.Pa$$w0rd");
            userManager = new DBUserManager(connectionString, user);

            //when
            userManager.Register();
        }

        /// <summary>
        /// This test method is designed to test the selection of all the contacts of an user.
        /// </summary>
        [TestMethod]
        public void SelectUserContacts_BasicCase_Success()
        {
            //given
            user = new User("userhavecontact@hotmail.com", "Pa$$w0rd") {Id = 5};
            userManager = new DBUserManager(connectionString, user);
            int expectedNumberOfContacts = 3;

            //when
            List<Contact> actualContacts = userManager.SelectUserContacts();

            //then
            Assert.AreEqual(expectedNumberOfContacts, actualContacts.Count);
        }

        /// <summary>
        /// This test method is designed to test the selection of all the contacts of an user when the user doesn't have any contact.
        /// </summary>
        [TestMethod]
        public void SelectUserContacts_UserDoesNotHaveContact_Success()
        {
            //given
            user = new User("userwithoutcontact@hotmail.com", "Pa$$w0rd") {Id = 6};
            userManager = new DBUserManager(connectionString, user);
            int expectedNumberOfContacts = 0;

            //when
            List<Contact> actualContacts = userManager.SelectUserContacts();

            //then
            Assert.AreEqual(expectedNumberOfContacts, actualContacts.Count);
        }

        /// <summary>
        /// This test method is designed to test the selection of all the conversation of an user.
        /// </summary>
        [TestMethod]
        public void SelectUserConversations_BasicCase_Success()
        {
            //given
            user = new User("userHaveConversations@cpnv.ch", "Pa$$w0rd") {Id = 9};
            userManager = new DBUserManager(connectionString, user);
            int expectedNumberOfConversations = 2;

            //when
            List<Conversation> actualContacts = userManager.SelectUserConversations();

            //then
            Assert.AreEqual(expectedNumberOfConversations, actualContacts.Count);
        }

        /// <summary>
        /// This test method is designed to test the selection of all the conversation of an user when the user doesn't have any contact.
        /// </summary>
        [TestMethod]
        public void SelectUserConversations_UserDoesNotHaveConversations_Success()
        {
            //given
            user = new User("userWithoutConversations@cpnv.ch", "Pa$$w0rd") {Id = 10};
            userManager = new DBUserManager(connectionString, user);
            int expectedNumberOfConversations = 0;

            //when
            List<Conversation> actualContacts = userManager.SelectUserConversations();

            //then
            Assert.AreEqual(expectedNumberOfConversations, actualContacts.Count);
        }

        /// <summary>
        /// This method cleans all variable after each test method.
        /// </summary>
        [TestCleanup]
        public void CleanUp()
        {
            userManager = null;
            user = null;
        }
    }
}