﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model.Database;
using Model.Database.Entity;

namespace TestChatMessenger
{
    /// <summary>
    /// This test class verifies operations to <see cref="DBMessageManager"/> class.
    /// </summary>
    /// <remarks>
    /// The database used is a test database and is created with the script `test_database.sql`.
    /// Make sure to run the sql script before each session, i.e. if there is a insert query and a unique constraint.
    /// </remarks>
    [TestClass]
    public class TestDBMessageManager
    {
        private const string connectionString = "SERVER=localhost;" +
                                                "DATABASE=test_ybznrn;" +
                                                "UID=test_ybznrn_tester;" +
                                                "PASSWORD=Pa$$w0rd";

        private Message message;
        private DBMessageManager messageManager;
        private User owner;
        private Conversation conversation;
        private DBConversationManager conversationManager;

        /// <summary>
        /// This method initializes variables before each test method.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            owner = new User("yannick.baudraz@cpnv.ch", "Pa$$w0rd");
            conversation = new Conversation(owner);
            conversationManager = new DBConversationManager(connectionString, conversation);
            message = new Message("Salut !", 2);
            messageManager = new DBMessageManager(connectionString, message);
        }

        /// <summary>
        /// This method tests when a message is added to the database with success.
        /// </summary>
        [TestMethod]
        public void AddMessage_RightInformation_Success()
        {
            //given
            conversation.Id = 1;

            //when
            messageManager.AddMessage(conversation, conversationManager);

            //then
            Assert.AreNotEqual(DateTime.MinValue, message.CreationDate);
            Assert.IsTrue(message.Id > 0);
            CollectionAssert.Contains(conversation.Messages, message);
            Assert.AreNotEqual(DateTime.MinValue, conversation.UpdateDate);
            Assert.IsTrue(DateTime.Compare(conversation.UpdateDate, conversation.CreationDate) > 0);
            // And verify entries in database
        }

        /// <summary>
        /// This method tests when a message is added but the user is not in the conversation given.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(DBMessageManager.MessageCannotBeAddedException))]
        public void AddMessage_UserDoesNotMatch_Exception()
        {
            //given
            conversation.Id = 1;
            message.SenderId = 12;

            //when
            messageManager.AddMessage(conversation, conversationManager);
        }

        /// <summary>
        /// This method tests when a message is added but the conversation doesn't exists.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(DBMessageManager.MessageCannotBeAddedException))]
        public void AddMessage_ConversationDoesNotExist_Exception()
        {
            //given
            conversation.Id = 12345;

            //when
            messageManager.AddMessage(conversation, conversationManager);
        }

        /// <summary>
        /// This methods tests when a message is deleted to the database and from the list that contains the message.
        /// </summary>
        [TestMethod]
        public void DeleteMessage_BasicCase_Success()
        {
            //given
            message.Id = 5;
            List<Message> messages = new List<Message>() {message};

            //when
            messageManager.DeleteMessage(messages);

            //then
            Assert.IsFalse(messages.Contains(message));
        }

        /// <summary>
        /// This method cleans all variable after each test method.
        /// </summary>
        [TestCleanup]
        public void CleanUp()
        {
            owner = null;
            messageManager = null;
            message = null;
            conversation = null;
            conversationManager = null;
        }
    }
}
