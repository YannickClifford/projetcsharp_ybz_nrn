﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model.Database.Entity;

namespace TestChatMessenger
{
    /// <summary>
    /// This test class verify operations to <see cref="Contact"/> class.
    /// </summary>
    [TestClass]
    public class TestContact
    {
        /// <summary>
        /// This method test the properties of the contact just after the instantiation.
        /// </summary>
        [TestMethod]
        public void NewContact_PropertiesAfterCreation_Success()
        {
            //given
            string email = "test@cpnv.ch";
            string nickname = "BFF <3";
            int userID = 2;
            Contact contact = new Contact(email, nickname, userID);

            //when
            string actualEmail = contact.Email;
            string actualNickname = contact.Nickname;
            int actualUserId = contact.UserId;

            //then
            Assert.AreEqual(email, actualEmail);
            Assert.AreEqual(nickname, actualNickname);
            Assert.AreEqual(userID, actualUserId);
        }
    }
}