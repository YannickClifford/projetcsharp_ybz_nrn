﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;

namespace TestChatMessenger
{
    /// <summary>
    /// This test class is designed to verify operations on <see cref="Settings"/> class.
    /// </summary>
    [TestClass]
    public class TestSettings
    {
        private const string settingsInJson = "{" +
                                              "\"WindowsY\":30," +
                                              "\"WindowsX\":25," +
                                              "\"DbConnectionString\":\"database:mirador\"," +
                                              "\"Language\":\"fr\"," +
                                              "\"UserEmail\":\"\"" +
                                              "}";

        /// <summary>
        /// This test method is designed to test the conversion from the class <see cref="Settings"/> to a Json string.
        /// </summary>
        [TestMethod]
        public void ToJson_BasicCase_Success()
        {
            //given
            Settings settings = Settings.Instance.FromJson(settingsInJson);

            //when
            string actualJson = settings.ToJson();

            //then
            Assert.AreEqual(settingsInJson, actualJson);
        }

        /// <summary>
        /// This test method is designed to test the conversion from a Json string to the class <see cref="Settings"/>.
        /// </summary>
        [TestMethod]
        public void FromJson_BasicCase_Success()
        {
            //given
            Settings actualSettings = Settings.Instance;

            //when
            actualSettings = actualSettings.FromJson(settingsInJson);

            //then
            string actualSettingsInJsonFormat = actualSettings.ToJson();
            Assert.AreEqual(settingsInJson, actualSettingsInJsonFormat);
        }
    }
}