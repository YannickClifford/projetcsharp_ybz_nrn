﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model.Database;
using Model.Database.Entity;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace TestChatMessenger
{
    /// <summary>
    /// This test class verify operations to <see cref="DBConversationManager"/> class.
    /// </summary>
    /// <remarks>
    /// The database used is a test database and is created with the script `test_database.sql`.
    /// Make sure to run the sql script before each session, i.e. if there is a insert query and a unique constraint.
    /// </remarks>
    [TestClass]
    public class TestDBConversationManager
    {
        private const string connectionString = "SERVER=localhost;" +
                                                "DATABASE=test_ybznrn;" +
                                                "UID=test_ybznrn_tester;" +
                                                "PASSWORD=Pa$$w0rd";

        private DBConversationManager conversationManager;
        private Conversation conversation;
        private User owner;
        private Contact otherParticipant;

        /// <summary>
        /// This method initialize variables before each test method.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            owner = new User("yannick.baudraz@cpnv.ch", "Pa$$w0rd") {Id = 2};
            new DBUserManager(connectionString, owner).Login();
            conversation = new Conversation(owner);
            conversationManager = new DBConversationManager(connectionString, conversation);
        }

        /// <summary>
        /// This method tests when a conversation is created to the database with success.
        /// </summary>
        [TestMethod]
        public void CreateConversation_RightInformation_Success()
        {
            //given
            otherParticipant = new Contact("conversationRightInfo@cpnv.ch", "Right info", owner.Id) {Id = 8};

            //when
            conversationManager.CreateConversation(otherParticipant);

            //then
            MySqlConnection con = new MySqlConnection(connectionString);
            con.Open();
            const string query = "SELECT conversation_id FROM conversation_participants " +
                                 "WHERE user_id IN (2, 8) " +
                                 "GROUP BY conversation_id HAVING COUNT(*) = 2";
            MySqlDataReader dr = new MySqlCommand(query, con).ExecuteReader();
            Assert.IsTrue(dr.Read());
            con.Close();

            CollectionAssert.Contains(owner.Conversations, conversation);
            Conversation actualOwnerConversation =
                owner.Conversations.Find(x => x.OtherParticipant == otherParticipant);
            Assert.IsNotNull(actualOwnerConversation);
            Assert.AreNotEqual(DateTime.MinValue, actualOwnerConversation.CreationDate);
        }

        /// <summary>
        /// This method test when a conversation is created to the database but the conversation already exists.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(DBConversationManager.ConversationAlreadyExistsException))]
        public void CreateConversation_ConversationAlreadyExists_Exception()
        {
            //given
            otherParticipant = new Contact("conversationexists@cpnv.ch", "conversationExists", owner.Id) {Id = 7};

            //when
            conversationManager.CreateConversation(otherParticipant);
        }

        /// <summary>
        /// This method test when a conversation is created to the database but the other participant of the conversation doesn't have an id.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(DBConversationManager.OtherParticipantWithoutIdException))]
        public void CreateConversation_OtherParticipantWithoutId_Exception()
        {
            //given
            otherParticipant = new Contact("conversationexists@cpnv.ch", "conversationExists", owner.Id);

            //when
            conversationManager.CreateConversation(otherParticipant);
        }

        /// <summary>
        /// This method test when the update date of the conversation when it's updated.
        /// </summary>
        [TestMethod]
        public void UpdateModificationDate_BasicCase_Success()
        {
            //given
            conversation.Id = 1;

            //when
            conversationManager.UpdateModificationDate();

            //then
            Assert.AreNotEqual(DateTime.MinValue, conversation.UpdateDate);
        }

        /// <summary>
        /// This test method is designed to test the selection of all the messages of a conversation.
        /// </summary>
        [TestMethod]
        public void SelectConversationMessages_BasicCase_Success()
        {
            //given
            conversation.Id = 2;
            const int expectedNumberOfMessages = 4;

            //when
            List<Message> actualMessagesOfConversation = conversationManager.SelectConversationMessages();

            //then
            Assert.AreEqual(expectedNumberOfMessages, actualMessagesOfConversation.Count);
        }

        /// <summary>
        /// This methods tests when an empty conversation is deleted to the database.
        /// </summary>
        [TestMethod]
        public void DeleteConversation_EmptyConversation_Success()
        {
            //given
            conversation.Id = 5;
            owner.Conversations.Add(conversation);

            //when
            conversationManager.DeleteConversation(owner.Conversations);

            //then
            CollectionAssert.DoesNotContain(owner.Conversations, conversation);
        }

        /// <summary>
        /// This methods tests when a conversation that contains messages in the database is deleted to the database.
        /// </summary>
        [TestMethod]
        public void DeleteConversation_ConversationWithMessagesInDTB_Success()
        {
            //given
            conversation.Id = 6;
            owner.Conversations.Add(conversation);

            //when
            conversationManager.DeleteConversation(owner.Conversations);

            //then
            CollectionAssert.DoesNotContain(owner.Conversations, conversation);
        }

        /// <summary>
        /// This method cleans all variable after each test method.
        /// </summary>
        [TestCleanup]
        public void CleanUp()
        {
            conversationManager = null;
            conversation = null;
            owner = null;
            otherParticipant = null;
        }
    }
}