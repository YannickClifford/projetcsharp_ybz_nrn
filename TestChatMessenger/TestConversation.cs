﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model.Database.Entity;
using System;

namespace TestChatMessenger
{
    /// <summary>
    /// This test class verify operations to <see cref="Conversation"/> class.
    /// </summary>
    [TestClass]
    public class TestConversation
    {
        private Conversation conversation;
        private User owner;

        /// <summary>
        /// This method initialize variables before each test method.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            owner = new User("test@cpnv.ch", "Pa$$w0rd");
            conversation = new Conversation(owner);
        }

        /// <summary>
        /// This method tests the properties of<see cref="Conversation"/> just after the instantiation.
        /// </summary>
        [TestMethod]
        public void NewConversation_PropertiesAfterCreation_Success()
        {
            //when
            User actualOwner = conversation.Owner;

            //then
            Assert.AreEqual(owner, actualOwner);
        }

        /// <summary>
        /// This method tests the accessors of <see cref="Conversation"/>.
        /// </summary>
        [TestMethod]
        public void Accessors_SetAndGet_Success()
        {
            //given
            const int id = 42;
            Contact otherParticipant = new Contact("contact@cpnv.ch", "Contact", 17);
            DateTime creationDate = DateTime.Now;
            DateTime updateDate = creationDate.AddHours(3);

            //when
            conversation.Id = id;
            conversation.OtherParticipant = otherParticipant;
            conversation.CreationDate = creationDate;
            conversation.UpdateDate = updateDate;

            //then
            Assert.AreEqual(id, conversation.Id);
            Assert.AreEqual(otherParticipant, conversation.OtherParticipant);
            Assert.AreEqual(creationDate, conversation.CreationDate);
            Assert.AreEqual(updateDate, conversation.UpdateDate);
        }

        /// <summary>
        /// This method cleans all fields after each test method.
        /// </summary>
        [TestCleanup]
        public void CleanUp()
        {
            owner = null;
            conversation = null;
        }
    }
}