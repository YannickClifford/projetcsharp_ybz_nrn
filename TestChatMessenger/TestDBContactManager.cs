﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model.Database;
using Model.Database.Entity;
using MySql.Data.MySqlClient;
using System.Linq;

namespace TestChatMessenger
{
    /// <summary>
    /// This test class verify operations to <see cref="DBContactManager"/> class.
    /// </summary>
    /// <remarks>
    /// The database used is a test database and is created with the script `test_database.sql`.
    /// Make sure to run the sql script before each session, i.e. if there is a insert query and a unique constraint.
    /// </remarks>
    [TestClass]
    public class TestDBContactManager
    {
        private const string connectionString = "SERVER=localhost;" +
                                                "DATABASE=test_ybznrn;" +
                                                "UID=test_ybznrn_tester;" +
                                                "PASSWORD=Pa$$w0rd";

        private DBContactManager contactManager;
        private Contact contact;
        private User user;

        /// <summary>
        /// This method initialize variables before each test method.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            user = new User("yannick.baudraz@cpnv.ch", "Pa$$w0rd");
            new DBUserManager(connectionString, user).Login();
        }

        /// <summary>
        /// This method tests when a contact is added to the database with success.
        /// </summary>
        [TestMethod]
        public void AddContact_RightInformation_Success()
        {
            //given
            contact = new Contact("test@cpnv.ch", "Tester", user.Id);
            contactManager = new DBContactManager(connectionString, contact);

            //when
            contactManager.AddContact(user);

            //then
            // Select the contact just created
            MySqlConnection con = new MySqlConnection(connectionString);
            con.Open();
            string query = "SELECT user_id FROM users_contacts WHERE nickname = 'Tester'";
            MySqlDataReader dr = new MySqlCommand(query, con).ExecuteReader();
            Assert.IsTrue(dr.Read());
            con.Close();
            // Verify the contact is added to the contact list
            Assert.AreEqual(contact, user.Contacts.Last());
            Assert.AreNotEqual(0, contact.Id);
        }

        /// <summary>
        /// This method tests when a contact is added to the database but his email is not matching.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(DBContactManager.EmailContactDoesNotMatchException))]
        public void AddContact_EmailContactDoesNotMatch_Exception()
        {
            //given
            contact = new Contact("NoEmail@cpnv.ch", "John", user.Id);
            contactManager = new DBContactManager(connectionString, contact);

            //when
            contactManager.AddContact(user);
        }

        /// <summary>
        /// This method tests when a contact is added to the database but his nickname is already used by another contact of the user and the email does not match with the database. 
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(DBContactManager.NicknameAlreadyUsedException))]
        public void AddContact_NicknameAlreadyUsedAndEmailDoesNotMatch_Exception()
        {
            //given
            contact = new Contact("tester@gmail.com", "Nate", user.Id);
            contactManager = new DBContactManager(connectionString, contact);

            //when
            contactManager.AddContact(user);
        }

        /// <summary>
        /// This method tests when a contact is added to the database but his nickname is already used by another contact of the user and the email matches with the database. 
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(DBContactManager.NicknameAlreadyUsedException))]
        public void AddContact_NicknameAlreadyUsedAndEmailMatches_Exception()
        {
            //given
            contact = new Contact("nathan.rayburn@cpnv.ch", "Mr. Robot", 1);
            contactManager = new DBContactManager(connectionString, contact);

            //when
            contactManager.AddContact(user);
        }

        /// <summary>
        /// This method tests when a contact is added to the database but his email is already used by another contact of the user. 
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(DBContactManager.EmailAlreadyUsedException))]
        public void AddContact_EmailAlreadyUsed_Exception()
        {
            //given
            contact = new Contact("nathan.rayburn@cpnv.ch", "Nathan", user.Id);
            contactManager = new DBContactManager(connectionString, contact);

            //when
            contactManager.AddContact(user);
        }

        /// <summary>
        /// This method test when a contact tries to add himself to the database.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(DBContactManager.UserAddingHimselfAsContactException))]
        public void AddContact_UserAddingHimselfAsContact_Exception()
        {
            //given
            contact = new Contact("yannick.baudraz@cpnv.ch", "Yannick", user.Id);
            contactManager = new DBContactManager(connectionString, contact);

            //when
            contactManager.AddContact(user);
        }

        /// <summary>
        /// This method cleans all variable after each test method.
        /// </summary>
        [TestCleanup]
        public void CleanUp()
        {
            contactManager = null;
            contact = null;
            user = null;
        }
    }
}