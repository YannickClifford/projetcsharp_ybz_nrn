﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model.Database.Entity;
using System.Collections.Generic;

namespace TestChatMessenger
{
    /// <summary>
    /// This test class verifies operations to <see cref="User"/> class.
    /// </summary>
    [TestClass]
    public class TestUser
    {
        private User user;
        private string email;
        private string password;

        /// <summary>
        /// This method initialize variables before each test method.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            email = "test@cpnv.ch";
            password = "wordOfPass";
            user = new User(email, password);
        }

        /// <summary>
        /// This method tests the properties of the user just after the instantiation.
        /// </summary>
        [TestMethod]
        public void NewUser_PropertiesAfterCreation_Success()
        {
            //given
            int id = 17;
            bool connected = true;
            user.Id = id;
            user.IsConnected = connected;

            //when
            string actualEmail = user.Email;
            string actualPassword = user.Password;
            int actualId = user.Id;
            bool actualConnectionState = user.IsConnected;

            //then
            Assert.AreEqual(email, actualEmail);
            Assert.AreEqual(password, actualPassword);
            Assert.AreEqual(id, actualId);
            Assert.AreEqual(connected, actualConnectionState);
        }

        /// <summary>
        /// This method tests when a contact is added to the contacts list of the user.
        /// </summary>
        [TestMethod]
        public void Contacts_AddContact_Success()
        {
            //given
            Contact contact = new Contact("contact@cpnv.ch", "John", user.Id);

            //when
            user.Contacts.Add(contact);

            //then
            CollectionAssert.Contains(user.Contacts, contact);
        }

        /// <summary>
        /// This method tests when a list that contains contacts are set to <see cref="User.Contacts"/>. It also tests the getter.
        /// </summary>
        [TestMethod]
        public void Contacts_SetListContainsContacts_Success()
        {
            //given
            Contact contact1 = new Contact("friend@cpnv.ch", "Michael", 3);
            Contact contact2 = new Contact("clifford@google.ch", "Clifford", 1);
            List<Contact> contactsList = new List<Contact> {contact1, contact2};

            //when
            user.Contacts = contactsList;

            //then
            CollectionAssert.AreEqual(contactsList, user.Contacts);
        }

        /// <summary>
        /// This method tests when a list that contains contacts are set to <see cref="User.Contacts"/>. It also tests the getter.
        /// </summary>
        [TestMethod]
        public void Conversation_SetListContainsConversations_Success()
        {
            //given
            Conversation conversation1 = new Conversation(new User("", ""));
            Conversation conversation2 = new Conversation(new User("", ""));
            List<Conversation> conversationList = new List<Conversation> {conversation1, conversation2};

            //when
            user.Conversations = conversationList;

            //then
            CollectionAssert.AreEqual(conversationList, user.Conversations);
        }

        /// <summary>
        /// This method cleans all variable after each test method.
        /// </summary>
        [TestCleanup]
        public void CleanUp()
        {
            email = "test@cpnv.ch";
            password = "wordOfPass";
            user = null;
        }
    }
}