﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model.Database.Entity;

namespace TestChatMessenger
{
    /// <summary>
    /// This test class verify operations to <see cref="Message"/> class.
    /// </summary>
    [TestClass]
    public class TestMessage
    {
        private Message message;
        private string content;
        private int senderId;

        /// <summary>
        /// This method initialize variables before each test method.
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            content = "Bonjour !";
            senderId = 2;
        }

        /// <summary>
        /// This method test the properties of the <see cref="Message"/> just after the instantiation.
        /// </summary>
        [TestMethod]
        public void NewMessage_PropertiesAfterCreation_Success()
        {
            //given
            content = "Bonjour !  \n";
            message = new Message(content, senderId);
            const string expectedContent = "Bonjour !";

            //when
            string actualContent = message.Content;
            int actualSenderId = message.SenderId;

            //then
            Assert.AreEqual(expectedContent, actualContent);
            Assert.AreEqual(senderId, actualSenderId);
        }

        /// <summary>
        /// This method test when a <see cref="Message"/> is created with an null content.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NewMessage_ContentNull_Exception()
        {
            //given
            content = null;
            const string expectedExceptionParamName = "content";

            try
            {
                //when
                message = new Message(content, senderId);
            }
            catch (ArgumentException e)
            {
                //then
                Assert.AreEqual(expectedExceptionParamName, e.ParamName.ToLower());
                throw;
            }
        }

        /// <summary>
        /// This method test when a <see cref="Message"/> is created with an empty content.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NewMessage_ContentEmpty_Exception()
        {
            //given
            content = "";
            const string expectedExceptionParamName = "content";

            try
            {
                //when
                message = new Message(content, senderId);
            }
            catch (ArgumentException e)
            {
                //then
                Assert.AreEqual(expectedExceptionParamName, e.ParamName.ToLower());
                throw;
            }
        }

        /// <summary>
        /// This method test when a <see cref="Message"/> is created with a content that contains only white spaces.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NewMessage_ContentWithWhiteSpacesOnly_Exception()
        {
            //given
            content = "     ";
            const string expectedExceptionParamName = "content";

            try
            {
                //when
                message = new Message(content, senderId);
            }
            catch (ArgumentException e)
            {
                //then
                Assert.AreEqual(expectedExceptionParamName, e.ParamName.ToLower());
                throw;
            }
        }

        /// <summary>
        /// This method test when a <see cref="Message"/> is created with a sender id less than 1.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NewMessage_SenderIdLessThan1_Exception()
        {
            //given
            senderId = 0;
            const string expectedExceptionParamName = "senderId";

            try
            {
                //when
                message = new Message(content, senderId);
            }
            catch (ArgumentException e)
            {
                //then
                string actualParamName = e.ParamName.First().ToString().ToLower() + e.ParamName.Substring(1);
                Assert.AreEqual(expectedExceptionParamName, actualParamName);
                throw;
            }
        }

        /// <summary>
        /// This method test when <see cref="Message.Id"/> is set with a number less than 1.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Id_LessThan1_Exception()
        {
            //given
            const string expectedExceptionParamName = "id";
            message = new Message(content, senderId);

            try
            {
                //when
                message.Id = 0;
            }
            catch (ArgumentException e)
            {
                //then
                Assert.AreEqual(expectedExceptionParamName, e.ParamName.ToLower());
                throw;
            }
        }

        /// <summary>
        /// This method cleans all variable after each test method.
        /// </summary>
        [TestCleanup]
        public void CleanUp()
        {
            message = null;
            content = null;
            senderId = int.MinValue;
        }
    }
}