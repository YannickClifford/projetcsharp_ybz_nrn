# Chat Messenger

Projet "Chat Messenger" C# dans le cadre du CPNV.

## Utilsation

Pour pouvoir utiliser correctement l'application il faut :

- Lancer l'installeur disponible sur le OneDrive
- Executer le fichier depuis le .exe ou le raccourci crée dans le bureau

## Dossiers

Le dossier _data_ contient le fichier de configuration Json et les scripts MySql qui sont dans le dossier _sql_

Les fichiers extraits par l'installeur sont placés dans le dossier "**C:\Users\\[user]\AppData\Roaming\YBZ_NRN\Chat Messenger**"

Les anciennes versions de l'applications se désinstallent lors de l'installation.

---

**Have fun !**
