﻿using Model.Database.Entity;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace Model.Database
{
    /// <summary>
    /// This class manages the class <see cref="Conversation"/> with the database.
    /// </summary>
    public class DBConversationManager : DatabaseConnector
    {
        private readonly Conversation _conversation;

        /// <summary>
        /// This constructor instantiates a <see cref="DBConversationManager"/>.
        /// </summary>
        /// <param name="connectionString">String fot the connection to the database.</param>
        /// <param name="conversation">Conversation to manage.</param>
        public DBConversationManager(string connectionString, Conversation conversation) : base(connectionString)
        {
            _conversation = conversation;
        }

        #region Public methods

        /// <summary>
        /// This method insert a new conversation between a user and his contact in the database.
        /// </summary>
        /// <param name="otherParticipant">Contact to add to the conversation after the insert.</param>
        /// <exception cref="ConversationAlreadyExistsException"></exception>
        public void CreateConversation(Contact otherParticipant)
        {
            if (otherParticipant.Id == 0) throw new OtherParticipantWithoutIdException();
            CheckIfConversationExists();
            InsertConversation();
            _conversation.OtherParticipant = otherParticipant;
            _conversation.Owner.Conversations.Add(_conversation);

            #region Local functions

            void CheckIfConversationExists()
            {
                string selectConversationQuery = "SELECT conversation_id FROM conversation_participants " +
                                                 $"WHERE user_id IN ({_conversation.Owner.Id}, {otherParticipant.Id}) " +
                                                 "GROUP BY conversation_id HAVING COUNT(*) = 2";
                MySqlDataReader dr = ExecuteQueryReader(selectConversationQuery);
                if (dr.Read()) throw new ConversationAlreadyExistsException();
                CloseConnection();
            }

            void InsertConversation()
            {
                _conversation.CreationDate = DateTime.Now;
                string insertConversationQuery = "INSERT INTO conversations (creation_date) " +
                                                 $"VALUES ('{_conversation.CreationDate:yyyy-MM-dd HH:mm:ss}'); SELECT LAST_INSERT_ID();";
                MySqlDataReader dr = ExecuteQueryReader(insertConversationQuery);
                dr.Read();
                int lastInsertedId = int.Parse(dr["LAST_INSERT_ID()"].ToString());
                _conversation.Id = lastInsertedId;
                CloseConnection();

                string insertTwoParticipantsQuery = $@" INSERT INTO
                                                            conversation_participants 
                                                        VALUES
	                                                        ({lastInsertedId}, {_conversation.Owner.Id}),
	                                                        ({lastInsertedId}, {otherParticipant.Id});";
                ExecuteQuery(insertTwoParticipantsQuery);
            }

            #endregion
        }

        /// <summary>
        /// Update the modification date of the conversation in the database.
        /// </summary>
        public void UpdateModificationDate()
        {
            CheckConversationId();
            DateTime updateDate = DateTime.Now;
            string updateModificationDateQuery = "UPDATE conversations " +
                                                 $"SET update_date = '{updateDate:yyyy-MM-dd HH:mm:ss}' " +
                                                 $"WHERE id = {_conversation.Id}";

            ExecuteQuery(updateModificationDateQuery);
            _conversation.UpdateDate = updateDate;
        }

        /// <summary>
        /// This method selects all the message of a conversation.
        /// </summary>
        /// <returns>A list of all the <see cref="Message"/> of the <see cref="Conversation"/>, sorted by date.</returns>
        public List<Message> SelectConversationMessages()
        {
            CheckConversationId();
            string selectMessageInConversationQuery = "SELECT id, user_id, creation_date, content " +
                                                      "FROM messages " +
                                                      $"WHERE conversation_id = {_conversation.Id}";
            MySqlDataReader dr = ExecuteQueryReader(selectMessageInConversationQuery);
            List<Message> messagesInConversation = new List<Message>();
            while (dr.Read())
                messagesInConversation.Add(new Message(dr["content"].ToString(),
                    int.Parse(dr["user_id"].ToString())
                ) {Id = int.Parse(dr["id"].ToString()), CreationDate = Convert.ToDateTime(dr["creation_date"])});
            CloseConnection();
            messagesInConversation.Sort((x, y) => DateTime.Compare(x.CreationDate, y.CreationDate));

            return messagesInConversation;
        }

        /// <summary>
        /// This methods deletes a conversation in the database.
        /// </summary>
        /// <param name="conversations">List of conversations that contains the conversation.</param>
        public void DeleteConversation(List<Conversation> conversations)
        {
            CheckConversationId();

            string query = $"DELETE FROM conversations WHERE id = {_conversation.Id}";
            ExecuteQuery(query);

            for (int i = 0; i < conversations.Count; i++)
            {
                if (_conversation.Id != conversations[i].Id) continue;
                conversations.Remove(_conversation);
                break;
            }
        }

        #endregion

        #region Private methods

        private void CheckConversationId()
        {
            if (_conversation.Id < 1) throw new Exception("The conversation does not have any id !");
        }

        #endregion

        #region Exceptions

        /// <summary>
        /// Exception when a conversation is tried to be created and the two users are already used for a conversation.
        /// </summary>
        public class ConversationAlreadyExistsException : Exception
        {
        }

        /// <summary>
        /// Exception when a conversation is tied to be created and the other participant of the conversation doesn't 
        /// </summary>
        public class OtherParticipantWithoutIdException : Exception
        {
        }

        #endregion
    }
}