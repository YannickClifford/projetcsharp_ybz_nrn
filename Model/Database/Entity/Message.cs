﻿using System;

namespace Model.Database.Entity
{
    /// <summary>
    /// This class represent a message in a conversation.
    /// </summary>
    public class Message
    {
        #region Fields and Accesors

        /// <summary>
        /// Id of the message
        /// </summary>
        public int Id
        {
            get => id;
            set => id = value < 1
                ? throw new ArgumentException("The message id cannot be less than 1.", nameof(Id))
                : value;
        }
        private int id;

        /// <summary>
        /// Content of the message.
        /// </summary>
        public string Content
        {
            get => content.Trim();
            private set => content = string.IsNullOrWhiteSpace(value)
                ? throw new ArgumentException(
                    "The content cannot be null, empty or whit white spaces only.",
                    nameof(Content))
                : value;
        }
        private string content;

        /// <summary>
        /// Date of the creation of the message.
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Id of the user who writes the message.
        /// </summary>
        public int SenderId
        {
            get => senderId;
            set => senderId = value < 1
                ? throw new ArgumentException("The id of the sender cannot be less than 1.", nameof(SenderId))
                : value;
        }
        private int senderId;

        #endregion

        /// <summary>
        /// This constructor instantiate a <see cref="Message"/> with his content and the user id.
        /// </summary>
        /// <param name="content">Content of the message</param>
        /// <param name="senderId">Id of the user who writes the message.</param>
        public Message(string content, int senderId)
        {
            Content = content;
            SenderId = senderId;
        }
    }
}