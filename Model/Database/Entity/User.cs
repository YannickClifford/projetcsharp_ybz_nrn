﻿using System.Collections.Generic;

namespace Model.Database.Entity
{
    /// <summary>
    /// This class represent an user.
    /// </summary>
    public class User
    {
        #region Properties

        /// <summary>
        /// User's unique identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// user's unique email.
        /// </summary>
        public string Email { get; }

        /// <summary>
        /// User's password.
        /// </summary>
        /// <remarks>The password is hashed.</remarks>
        public string Password { get; }

        /// <summary>
        /// If the user is connected to the application or not.
        /// </summary>
        public bool IsConnected { get; set; }

        /// <summary>
        /// User's list of contact.
        /// </summary>
        public List<Contact> Contacts { get; set; }

        /// <summary>
        /// User's list of conversation.
        /// </summary>
        public List<Conversation> Conversations { get; set; }

        #endregion

        /// <summary>
        /// This constructor instantiate an user with an email and a password.
        /// </summary>
        /// <param name="email">The unique email</param>
        /// <param name="password">The password.</param>
        public User(string email, string password)
        {
            Email = email;
            Password = password;
            Contacts = new List<Contact>();
            Conversations = new List<Conversation>();
        }
    }
}