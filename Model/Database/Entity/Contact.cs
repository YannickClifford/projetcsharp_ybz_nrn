﻿namespace Model.Database.Entity
{
    /// <summary>
    /// This class represent a contact of an user.
    /// </summary>
    public class Contact
    {
        #region Properties

        /// <summary>
        /// Contact's unique identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Contact's unique email.
        /// </summary>
        public string Email { get; }

        /// <summary>
        /// Contact's nickname.
        /// </summary>
        public string Nickname { get; }

        /// <summary>
        /// Id of the user who possesses the contact.
        /// </summary>
        public int UserId { get; }

        #endregion

        /// <summary>
        /// This constructor instantiate a <see cref="Contact"/> with an email and a nickname.
        /// </summary>
        /// <param name="email">Email of the contact.</param>
        /// <param name="nickname">Nickname of the contact</param>
        /// <param name="userId">Id of the user who possesses the contact.</param>
        public Contact(string email, string nickname, int userId)
        {
            Email = email;
            Nickname = nickname;
            UserId = userId;
        }
    }
}