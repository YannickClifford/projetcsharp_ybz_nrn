﻿using System;
using System.Collections.Generic;

namespace Model.Database.Entity
{
    /// <summary>
    /// This class represent a conversation of an user.
    /// </summary>
    public class Conversation
    {
        #region Properties

        /// <summary>
        /// Id of the conversation.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// User who be a part of the conversation.
        /// </summary>
        public User Owner { get; }

        /// <summary>
        /// Contact of the user who be a part of the conversation.
        /// </summary>
        public Contact OtherParticipant { get; set; }

        /// <summary>
        /// Date when the conversation was created.
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Date of the last update of the conversation.
        /// </summary>
        public DateTime UpdateDate { get; set; }

        /// <summary>
        /// Messages in the conversation.
        /// </summary>
        public List<Message> Messages { get; set; } = new List<Message>();

        #endregion

        /// <summary>
        /// This constructor instantiate a <see cref="Conversation"/> with his owner.
        /// </summary>
        /// <param name="owner">User who be a part of the conversation.</param>
        public Conversation(User owner) => Owner = owner;
    }
}