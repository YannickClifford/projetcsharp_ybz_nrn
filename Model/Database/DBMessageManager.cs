﻿using System;
using System.Collections.Generic;
using Model.Database.Entity;
using MySql.Data.MySqlClient;

namespace Model.Database
{
    /// <summary>
    /// This class manages the class <see cref="Message"/> with the database.
    /// </summary>
    public class DBMessageManager : DatabaseConnector
    {
        private readonly Message message;

        /// <summary>
        /// This constructor initializes a <see cref="DBContactManager"/>.
        /// </summary>
        /// <param name="connectionString">String for the connection to the database.</param>
        /// <param name="message"><see cref="Message"/> to manage.</param>
        public DBMessageManager(string connectionString, Message message) : base(connectionString)
        {
            this.message = message;
        }

        #region Public methods

        /// <summary>
        /// This method insert a new message of a conversation in the database.
        /// </summary>
        /// <param name="conversation">Conversation that take the message.</param>
        /// <param name="conversationManager">Database manager of the conversation passed in parameter.</param>
        public void AddMessage(Conversation conversation, DBConversationManager conversationManager)
        {
            if (conversation.Id < 0) throw new Exception("The conversation does not have any id");

            message.CreationDate = DateTime.Now;
            string query = "INSERT INTO messages (conversation_id, user_id, content, creation_date) " +
                           $"VALUES ({conversation.Id}, {message.SenderId}, @content, '{message.CreationDate:yyyy-MM-dd HH:mm:ss}'); " +
                           "SELECT LAST_INSERT_ID();";
            MySqlParameter contentParam = new MySqlParameter("@content", message.Content);

            try
            {
                MySqlDataReader dr = ExecuteQueryReader(query, contentParam);
                dr.Read();
                int lastInsertedId = int.Parse(dr["LAST_INSERT_ID()"].ToString());
                CloseConnection();
                message.Id = lastInsertedId;
                conversation.Messages.Add(message);
                conversationManager.UpdateModificationDate();
            }
            catch (MySqlException e)
            {
                if (e.Number == 1452) throw new MessageCannotBeAddedException();
                throw;
            }
        }

        /// <summary>
        /// Delete the message to the database and from the list that contains the message.
        /// </summary>
        /// <param name="messages">List of message that contains the message.</param>
        public void DeleteMessage(List<Message> messages)
        {
            if (message.Id < 0) throw new Exception("The message does not have any id");

            string query = $"DELETE FROM messages WHERE id = {message.Id}";
            ExecuteQuery(query);

            for (int i = 0; i < messages.Count; i++)
            {
                if (message.Id != messages[i].Id) continue;
                messages.Remove(message);
                break;
            }
        }

        #endregion

        /// <summary>
        /// Exception when the message cannot be added.
        /// </summary>
        public class MessageCannotBeAddedException : Exception
        {
        }
    }
}
