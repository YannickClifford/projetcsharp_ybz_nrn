﻿using MySql.Data.MySqlClient;
using System.Data;

namespace Model.Database
{
    /// <summary>
    /// This class is a generic connector to the database.
    /// </summary>
    public abstract class DatabaseConnector
    {
        #region Private attributes

        private readonly string _connectionString;
        private MySqlConnection _connection;

        #endregion

        /// <summary>
        /// This constructor instantiate the database connection with the connection string.
        /// </summary>
        /// <param name="connectionString">The string that allow the connection to the database.</param>
        protected DatabaseConnector(string connectionString)
        {
            _connectionString = connectionString;
            _connection = new MySqlConnection();
        }

        #region Protected methods

        /// <summary>
        /// This methods execute a query that doesn't returns any row.<para/>
        /// Insert, Update, Delete...
        /// </summary>
        /// <param name="query">The MySQL query string to execute.</param>
        /// <param name="parameters">Optional parameters to a MySqlCommand.</param>
        /// <returns>The last id inserted if it was an auto-increment.</returns>
        protected void ExecuteQuery(string query, params MySqlParameter[] parameters)
        {
            OpenConnection();

            MySqlCommand command = new MySqlCommand(query, _connection);
            foreach (MySqlParameter param in parameters) command.Parameters.Add(param);
            command.Prepare();

            command.ExecuteNonQuery();

            CloseConnection();
        }

        /// <summary>
        /// This methods execute a query that return normally a row.
        /// </summary>
        /// <param name="query">The MySQL query string to execute.</param>
        /// <param name="parameters">Optional parameters to a MySqlCommand.</param>
        /// <returns>The result of the query.</returns>
        /// <remarks>Do not forget to dispose the returned object and close the connection when finish implementation.</remarks>
        protected MySqlDataReader ExecuteQueryReader(string query, params MySqlParameter[] parameters)
        {
            OpenConnection();

            MySqlCommand command = new MySqlCommand(query, _connection);
            foreach (MySqlParameter param in parameters) command.Parameters.Add(param);
            command.Prepare();

            return command.ExecuteReader();
        }

        /// <summary>
        /// Open the connection to the database and keep it in the memory.
        /// </summary>
        private void OpenConnection()
        {
            if (_connection.State != ConnectionState.Closed) return;
            _connection = new MySqlConnection(_connectionString);
            _connection.Open();
        }

        /// <summary>
        /// Close the connection to the database and clean the memory.
        /// </summary>
        protected void CloseConnection()
        {
            if (_connection.State == ConnectionState.Closed) return;
            _connection.Close();
        }

        #endregion
    }
}