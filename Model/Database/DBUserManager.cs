﻿using Model.Database.Entity;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using BCryptNet = BCrypt.Net;

namespace Model.Database
{
    /// <summary>
    /// This class manages the class <see cref="User"/> with the database.
    /// </summary>
    public class DBUserManager : DatabaseConnector
    {
        private readonly User _user;

        /// <summary>
        /// This constructor instantiate the connection to the database and take the <see cref="User"/> to manage.
        /// </summary>
        /// <param name="connectionString">String fot the connection to the database.</param>
        /// <param name="user">User to manage.</param>
        public DBUserManager(string connectionString, User user) : base(connectionString)
        {
            _user = user;
        }

        #region Public methods

        /// <summary>
        /// This method connect the user if his info match with the database.
        /// </summary>
        /// <exception cref="LoginFailException"></exception>
        public void Login()
        {
            const string query = "SELECT id, email, password FROM users WHERE email = @email";
            MySqlParameter emailParam = new MySqlParameter("@email", _user.Email);

            MySqlDataReader data = ExecuteQueryReader(query, emailParam);
            if (!data.Read() || !BCryptNet.BCrypt.Verify(_user.Password, data["password"].ToString()))
                throw new LoginFailException();

            _user.IsConnected = true;
            _user.Id = int.Parse(data["id"].ToString());

            CloseConnection();
        }

        /// <summary>
        /// This method register the user.
        /// </summary>
        /// <exception cref="UserAlreadyExistsException"></exception>
        public void Register()
        {
            string hashedPassword = BCryptNet.BCrypt.HashPassword(_user.Password);

            const string query = "INSERT INTO users (email, password) VALUES (@email, @password)";
            MySqlParameter emailParam = new MySqlParameter("@email", _user.Email);
            MySqlParameter passwordParam = new MySqlParameter("@password", hashedPassword);

            try
            {
                ExecuteQuery(query, emailParam, passwordParam);
            }
            catch (MySqlException exception)
            {
                if (exception.Number == 1062)
                    throw new UserAlreadyExistsException();

                throw;
            }
        }

        /// <summary>
        /// This method selects all the contacts of the user.
        /// </summary>
        /// <returns>A list of all the contacts of the user</returns>
        public List<Contact> SelectUserContacts()
        {
            List<Contact> contacts = new List<Contact>();
            string selectUserContactsQuery = "SELECT users.email, users_contacts.nickname, users_contacts.contact_id " +
                                             "FROM users INNER JOIN users_contacts ON users.id = users_contacts.contact_id " +
                                             $"WHERE users_contacts.user_id = {_user.Id}";

            MySqlDataReader dr = ExecuteQueryReader(selectUserContactsQuery);
            while (dr.Read())
                contacts.Add(new Contact(
                    dr["email"].ToString(),
                    dr["nickname"].ToString(),
                    _user.Id
                ) {Id = int.Parse(dr["contact_id"].ToString())});
            CloseConnection();

            return contacts;
        }

        /// <summary>
        /// This method selects all the conversations of the user.
        /// </summary>
        /// <returns>A list of all the conversations of the user with other users his added as contact.</returns>
        public List<Conversation> SelectUserConversations()
        {
            List<Conversation> conversations = new List<Conversation>();
            string selectInfoQuery = "SELECT " +
                                     "   conversations.id," +
                                     "   conversations.creation_date," +
                                     "   conversations.update_date," +
                                     "   users.email," +
                                     "   users_contacts.nickname," +
                                     "   users_contacts.contact_id " +
                                     "FROM users " +
                                     "   INNER JOIN users_contacts ON users.id = users_contacts.contact_id " +
                                     "   INNER JOIN conversation_participants ON users.id = conversation_participants.user_id " +
                                     "   INNER JOIN conversations ON conversations.id = conversation_participants.conversation_id " +
                                     "WHERE conversations.id IN (" +
                                     "   SELECT id FROM conversations " +
                                     "   INNER JOIN conversation_participants ON conversations.id = conversation_participants.conversation_id " +
                                     $"  WHERE user_id = {_user.Id}" +
                                     $") AND users_contacts.user_id = {_user.Id};";

            MySqlDataReader dr = ExecuteQueryReader(selectInfoQuery);
            while (dr.Read())
                conversations.Add(new Conversation(_user)
                {
                    Id = int.Parse(dr["id"].ToString()),
                    OtherParticipant = new Contact(
                            dr["email"].ToString(),
                            dr["nickname"].ToString(),
                            _user.Id
                        )
                        {Id = int.Parse(dr["contact_id"].ToString())},
                    CreationDate = Convert.ToDateTime(dr["creation_date"]),
                    UpdateDate = DateTime.TryParse(dr["update_date"].ToString(), out DateTime updateDate)
                        ? updateDate
                        : DateTime.MinValue
                });
            CloseConnection();

            return conversations;
        }

        #endregion

        #region Exceptions

        /// <summary>
        /// Exception when the login fail because the email isn't found or the password doesn't match.
        /// </summary>
        public class LoginFailException : Exception
        {
        }

        /// <summary>
        /// Exception when a user is already in the database.
        /// </summary>
        public class UserAlreadyExistsException : Exception
        {
        }

        #endregion
    }
}