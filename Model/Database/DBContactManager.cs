﻿using Model.Database.Entity;
using MySql.Data.MySqlClient;
using System;

namespace Model.Database
{
    /// <summary>
    /// This class manages the class <see cref="Contact"/> with the database.
    /// </summary>
    public class DBContactManager : DatabaseConnector
    {
        private readonly Contact _contact;

        /// <summary>
        /// This constructor initializes a <see cref="DBContactManager"/> .
        /// </summary>
        /// <param name="connectionString">String for the connection to the database.</param>
        /// <param name="contact">Contact to manage.</param>
        public DBContactManager(string connectionString, Contact contact) : base(connectionString)
        {
            _contact = contact;
        }

        /// <summary>
        /// This method insert a new contact of an user in the database.
        /// </summary>
        /// <param name="user">User who will possesses the contact.</param>
        /// <exception cref="NicknameAlreadyUsedException"></exception>
        /// <exception cref="UserAddingHimselfAsContactException"></exception>
        /// <exception cref="EmailAlreadyUsedException"></exception>
        /// <exception cref="EmailContactDoesNotMatchException"></exception>
        public void AddContact(User user)
        {
            if (CheckIfNicknameIsTaken())
                throw new NicknameAlreadyUsedException();
            InsertContact(SelectContactIdByEmail());


            #region Local functions

            bool CheckIfNicknameIsTaken()
            {
                string selectNicknameByUserQuery = "SELECT nickname FROM users_contacts " +
                                                   $"WHERE user_id = {_contact.UserId} AND nickname = @nicknameAlreadyUsed";
                MySqlParameter nicknameAlreadyUsedParam = new MySqlParameter("@nicknameAlreadyUsed", _contact.Nickname);

                bool isContainsData = ExecuteQueryReader(selectNicknameByUserQuery, nicknameAlreadyUsedParam).Read();
                CloseConnection();

                return isContainsData;
            }

            uint SelectContactIdByEmail()
            {
                string selectContactIdQuery = "SELECT id FROM users WHERE email = @email";
                MySqlParameter emailParam = new MySqlParameter("@email", _contact.Email);
                MySqlDataReader dr = ExecuteQueryReader(selectContactIdQuery, emailParam);
                if (!dr.Read())
                    throw new EmailContactDoesNotMatchException();
                uint id = (uint) dr["id"];
                CloseConnection();
                return id;
            }

            void InsertContact(uint contactId)
            {
                string insertContactQuery = "INSERT INTO users_contacts (user_id, contact_id, nickname) " +
                                            $"VALUES ({_contact.UserId}, {contactId}, @nickname)";
                MySqlParameter nicknameParam = new MySqlParameter("@nickname", _contact.Nickname);

                try
                {
                    ExecuteQuery(insertContactQuery, nicknameParam);
                    _contact.Id = (int) contactId;
                    user.Contacts.Add(_contact);
                }
                catch (MySqlException e)
                {
                    if (e.Number == 3819)
                        throw new UserAddingHimselfAsContactException();
                    if (e.Message.Contains("PRIMARY"))
                        throw new EmailAlreadyUsedException();
                }
            }

            #endregion
        }

        #region Exceptions

        /// <summary>
        /// Exception when a contact is tried to be added and the email doesn't match.
        /// </summary>
        public class EmailContactDoesNotMatchException : Exception
        {
        }

        /// <summary>
        /// Exception when a contact is tried to be added and the nickname is already used by another contact of the user.
        /// </summary>
        public class NicknameAlreadyUsedException : Exception
        {
        }

        /// <summary>
        /// Exception when a contact is tried to be added a contact and the email is already used by another contact of the user.
        /// </summary>
        public class EmailAlreadyUsedException : Exception
        {
        }

        /// <summary>
        /// Exception when the user tries to add himself as contact.
        /// </summary>
        public class UserAddingHimselfAsContactException : Exception
        {
        }

        #endregion
    }
}
