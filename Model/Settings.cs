﻿using Newtonsoft.Json;

namespace Model
{
    /// <summary>
    /// The user's settings for the runtime application.
    /// </summary>
    [JsonObject]
    public class Settings
    {
        #region Properties

        /// <summary>
        /// The location Y of the left-top corner of the windows.
        /// </summary>
        public int WindowsY { get; set; } = -1;

        /// <summary>
        /// The location X of the left-top corner of the windows.
        /// </summary>
        public int WindowsX { get; set; } = -1;

        /// <summary>
        /// The string to connect to the database.
        /// </summary>
        public string DbConnectionString { get; set; } = "";

        /// <summary>
        /// Language of the application.
        /// </summary>
        public string Language { get; set; } = "";

        /// <summary>
        /// Email of the last user connected if he checked "Remember me" button.
        /// </summary>
        public string UserEmail { get; set; } = "";

        #endregion

        private static Settings _instance;

        private Settings()
        {
        }

        /// <summary>
        /// The single instance of the class.
        /// </summary>
        public static Settings Instance => _instance ?? (_instance = new Settings());

        #region Public methods

        /// <summary>
        /// This methods transforms a JSON string to the class <see cref="Settings"/> herself.
        /// </summary>
        /// <param name="jsonString">The JSON string to transform into the Settings class.</param>
        /// <remarks>JSON fields need to be exactly like the accessors name.</remarks>
        public Settings FromJson(string jsonString) => _instance = JsonConvert.DeserializeObject<Settings>(jsonString);

        /// <summary>
        /// This method transforms the class <see cref="Settings"/> to a JSON string.
        /// </summary>
        /// <returns>A JSON string representation of the object.</returns>
        public string ToJson() => JsonConvert.SerializeObject(this);

        #endregion
    }
}