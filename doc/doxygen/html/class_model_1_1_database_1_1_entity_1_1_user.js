var class_model_1_1_database_1_1_entity_1_1_user =
[
    [ "User", "class_model_1_1_database_1_1_entity_1_1_user.html#a4948b775b26ca985178cb652fe476372", null ],
    [ "Contacts", "class_model_1_1_database_1_1_entity_1_1_user.html#a1ebdab2b9f88e5eaf69df3b77de354ca", null ],
    [ "Conversations", "class_model_1_1_database_1_1_entity_1_1_user.html#a7dd1227a1762d29ec51b7eaca055275e", null ],
    [ "Email", "class_model_1_1_database_1_1_entity_1_1_user.html#a466d78899e873627b92162699c9288e9", null ],
    [ "Id", "class_model_1_1_database_1_1_entity_1_1_user.html#a35fe63c29d8d48e6a23dc2308f069e6f", null ],
    [ "IsConnected", "class_model_1_1_database_1_1_entity_1_1_user.html#a2a08aa32a3c45c836f6d28dfab8fd6ab", null ],
    [ "Password", "class_model_1_1_database_1_1_entity_1_1_user.html#ab72c8e96ef5b8b30959fc358aa111fdf", null ]
];