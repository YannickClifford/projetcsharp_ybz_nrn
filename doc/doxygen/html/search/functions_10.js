var searchData=
[
  ['ucbubble_450',['ucBubble',['../class_chat_messenger_g_u_i_1_1uc_bubble.html#a4f3ac0d466efbe543995f61e2f009211',1,'ChatMessengerGUI.ucBubble.ucBubble()'],['../class_chat_messenger_g_u_i_1_1uc_bubble.html#abf810a92498e45972d1782677852e1ed',1,'ChatMessengerGUI.ucBubble.ucBubble(User user, string message, string time, MsgType messagetype, string sender)']]],
  ['ucconversation_451',['ucConversation',['../class_chat_messenger_g_u_i_1_1uc_conversation.html#a1f9639e75a672174d0f5c3e34b2c175d',1,'ChatMessengerGUI::ucConversation']]],
  ['uclogin_452',['ucLogin',['../class_chat_messenger_g_u_i_1_1uc_login.html#a7ea06cb37b451e1c8e6e3d4046cf9c2d',1,'ChatMessengerGUI.ucLogin.ucLogin()'],['../class_chat_messenger_g_u_i_1_1uc_login.html#ae9ba6bd9b7170f10e14db5d0f510c135',1,'ChatMessengerGUI.ucLogin.ucLogin(string tempEmail)']]],
  ['ucmain_453',['ucMain',['../class_chat_messenger_g_u_i_1_1uc_main.html#ad6de8fb5589371dd0e8ff42055ad22c0',1,'ChatMessengerGUI::ucMain']]],
  ['ucregister_454',['ucRegister',['../class_chat_messenger_g_u_i_1_1uc_register.html#aca2dc4f421237b60840d2b9c59ee8ac4',1,'ChatMessengerGUI::ucRegister']]],
  ['ucvalidate_455',['ucValidate',['../class_chat_messenger_g_u_i_1_1uc_validate.html#aaad05685c2e9d763aae2adbdea3659f6',1,'ChatMessengerGUI.ucValidate.ucValidate()'],['../class_chat_messenger_g_u_i_1_1uc_validate.html#a55b15c2976967a2e8fb717c51f1410bf',1,'ChatMessengerGUI.ucValidate.ucValidate(string userEmail)']]],
  ['updatemodificationdate_456',['UpdateModificationDate',['../class_model_1_1_database_1_1_d_b_conversation_manager.html#a132a72e821c7e196c967c701a98e1072',1,'Model::Database::DBConversationManager']]],
  ['updatemodificationdate_5fbasiccase_5fsuccess_457',['UpdateModificationDate_BasicCase_Success',['../class_test_chat_messenger_1_1_test_d_b_conversation_manager.html#a8bf05762218eb172cd92b79450b43b9c',1,'TestChatMessenger::TestDBConversationManager']]],
  ['user_458',['User',['../class_model_1_1_database_1_1_entity_1_1_user.html#a4948b775b26ca985178cb652fe476372',1,'Model::Database::Entity::User']]]
];
