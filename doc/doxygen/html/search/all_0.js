var searchData=
[
  ['accessors_5fsetandget_5fsuccess_0',['Accessors_SetAndGet_Success',['../class_test_chat_messenger_1_1_test_conversation.html#a03d7aefa91795e86f14666d042f80d61',1,'TestChatMessenger::TestConversation']]],
  ['addcontact_1',['AddContact',['../class_model_1_1_database_1_1_d_b_contact_manager.html#a7be5a52649f1ab028b939d950b2b9585',1,'Model::Database::DBContactManager']]],
  ['addcontact_5femailalreadyused_5fexception_2',['AddContact_EmailAlreadyUsed_Exception',['../class_test_chat_messenger_1_1_test_d_b_contact_manager.html#aca42ffd6c0915fa4e551170ee57aa408',1,'TestChatMessenger::TestDBContactManager']]],
  ['addcontact_5femailcontactdoesnotmatch_5fexception_3',['AddContact_EmailContactDoesNotMatch_Exception',['../class_test_chat_messenger_1_1_test_d_b_contact_manager.html#ab98506ae496588ccf6d22b3ef55723f1',1,'TestChatMessenger::TestDBContactManager']]],
  ['addcontact_5fnicknamealreadyusedandemaildoesnotmatch_5fexception_4',['AddContact_NicknameAlreadyUsedAndEmailDoesNotMatch_Exception',['../class_test_chat_messenger_1_1_test_d_b_contact_manager.html#a76fcb960b006645d62b868bbfc35e6dd',1,'TestChatMessenger::TestDBContactManager']]],
  ['addcontact_5fnicknamealreadyusedandemailmatches_5fexception_5',['AddContact_NicknameAlreadyUsedAndEmailMatches_Exception',['../class_test_chat_messenger_1_1_test_d_b_contact_manager.html#a6b842aaad6f2d168ef62871c6a4e09a1',1,'TestChatMessenger::TestDBContactManager']]],
  ['addcontact_5frightinformation_5fsuccess_6',['AddContact_RightInformation_Success',['../class_test_chat_messenger_1_1_test_d_b_contact_manager.html#a32d0f017a96f363f9c11ff6b2631aba6',1,'TestChatMessenger::TestDBContactManager']]],
  ['addcontact_5fuseraddinghimselfascontact_5fexception_7',['AddContact_UserAddingHimselfAsContact_Exception',['../class_test_chat_messenger_1_1_test_d_b_contact_manager.html#a021bc3ae0413114c17bd016a379d9b9f',1,'TestChatMessenger::TestDBContactManager']]],
  ['addmessage_8',['AddMessage',['../class_model_1_1_database_1_1_d_b_message_manager.html#a18ee18f350c0a29d03edb08db4c2a36f',1,'Model::Database::DBMessageManager']]],
  ['addmessage_5fconversationdoesnotexist_5fexception_9',['AddMessage_ConversationDoesNotExist_Exception',['../class_test_chat_messenger_1_1_test_d_b_message_manager.html#afad41b638d66d754f451866bce7eec1b',1,'TestChatMessenger::TestDBMessageManager']]],
  ['addmessage_5frightinformation_5fsuccess_10',['AddMessage_RightInformation_Success',['../class_test_chat_messenger_1_1_test_d_b_message_manager.html#a5b55d7da23e04d6ca84c14121be3b3f4',1,'TestChatMessenger::TestDBMessageManager']]],
  ['addmessage_5fuserdoesnotmatch_5fexception_11',['AddMessage_UserDoesNotMatch_Exception',['../class_test_chat_messenger_1_1_test_d_b_message_manager.html#a9a95819d5506db6458def0b230c88832',1,'TestChatMessenger::TestDBMessageManager']]],
  ['assemblyinfo_2ecs_12',['AssemblyInfo.cs',['../_chat_messenger_g_u_i_2_properties_2_assembly_info_8cs.html',1,'(Global Namespace)'],['../_test_chat_messenger_2_properties_2_assembly_info_8cs.html',1,'(Global Namespace)']]]
];
