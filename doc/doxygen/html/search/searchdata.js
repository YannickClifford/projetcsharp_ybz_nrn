var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstuvw",
  1: "bcdefhlmnopstu",
  2: "bcmt",
  3: "abcdfhlmprstu",
  4: "abcdefghilmnprstuv",
  5: "ilp",
  6: "bm",
  7: "hmp",
  8: "bilmnosvw",
  9: "cdeilmnoprsuvw",
  10: "clt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "properties",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Properties",
  10: "Pages"
};

