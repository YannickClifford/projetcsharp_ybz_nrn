var searchData=
[
  ['newcontact_5fpropertiesaftercreation_5fsuccess_427',['NewContact_PropertiesAfterCreation_Success',['../class_test_chat_messenger_1_1_test_contact.html#abf2af34e66fe565c5d54cc08fe65043f',1,'TestChatMessenger::TestContact']]],
  ['newconversation_5fpropertiesaftercreation_5fsuccess_428',['NewConversation_PropertiesAfterCreation_Success',['../class_test_chat_messenger_1_1_test_conversation.html#a49ca0559cd84e95eb055468500787fa0',1,'TestChatMessenger::TestConversation']]],
  ['newmessage_5fcontentempty_5fexception_429',['NewMessage_ContentEmpty_Exception',['../class_test_chat_messenger_1_1_test_message.html#a264c002541e0a8cfba3e75bedf6c9bf4',1,'TestChatMessenger::TestMessage']]],
  ['newmessage_5fcontentnull_5fexception_430',['NewMessage_ContentNull_Exception',['../class_test_chat_messenger_1_1_test_message.html#a7a2194c232f5efd83e86b9d331a67b34',1,'TestChatMessenger::TestMessage']]],
  ['newmessage_5fcontentwithwhitespacesonly_5fexception_431',['NewMessage_ContentWithWhiteSpacesOnly_Exception',['../class_test_chat_messenger_1_1_test_message.html#a7f61f9b83146ffd29c52fe739c9b501c',1,'TestChatMessenger::TestMessage']]],
  ['newmessage_5fpropertiesaftercreation_5fsuccess_432',['NewMessage_PropertiesAfterCreation_Success',['../class_test_chat_messenger_1_1_test_message.html#adf85dbf9472ea319efee97bf967cb1e0',1,'TestChatMessenger::TestMessage']]],
  ['newmessage_5fsenderidlessthan1_5fexception_433',['NewMessage_SenderIdLessThan1_Exception',['../class_test_chat_messenger_1_1_test_message.html#ac20c772ae035954c47747738ce8f3c50',1,'TestChatMessenger::TestMessage']]],
  ['newuser_5fpropertiesaftercreation_5fsuccess_434',['NewUser_PropertiesAfterCreation_Success',['../class_test_chat_messenger_1_1_test_user.html#aa7b9e4845847f7940fc5781aa7109dde',1,'TestChatMessenger::TestUser']]]
];
