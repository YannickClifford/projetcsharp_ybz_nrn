var namespace_test_chat_messenger =
[
    [ "TestContact", "class_test_chat_messenger_1_1_test_contact.html", "class_test_chat_messenger_1_1_test_contact" ],
    [ "TestConversation", "class_test_chat_messenger_1_1_test_conversation.html", "class_test_chat_messenger_1_1_test_conversation" ],
    [ "TestDBContactManager", "class_test_chat_messenger_1_1_test_d_b_contact_manager.html", "class_test_chat_messenger_1_1_test_d_b_contact_manager" ],
    [ "TestDBConversationManager", "class_test_chat_messenger_1_1_test_d_b_conversation_manager.html", "class_test_chat_messenger_1_1_test_d_b_conversation_manager" ],
    [ "TestDBMessageManager", "class_test_chat_messenger_1_1_test_d_b_message_manager.html", "class_test_chat_messenger_1_1_test_d_b_message_manager" ],
    [ "TestDBUserManager", "class_test_chat_messenger_1_1_test_d_b_user_manager.html", "class_test_chat_messenger_1_1_test_d_b_user_manager" ],
    [ "TestMessage", "class_test_chat_messenger_1_1_test_message.html", "class_test_chat_messenger_1_1_test_message" ],
    [ "TestPasswordCheck", "class_test_chat_messenger_1_1_test_password_check.html", "class_test_chat_messenger_1_1_test_password_check" ],
    [ "TestSettings", "class_test_chat_messenger_1_1_test_settings.html", "class_test_chat_messenger_1_1_test_settings" ],
    [ "TestUser", "class_test_chat_messenger_1_1_test_user.html", "class_test_chat_messenger_1_1_test_user" ]
];