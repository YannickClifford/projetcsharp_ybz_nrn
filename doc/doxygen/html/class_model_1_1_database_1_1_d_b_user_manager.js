var class_model_1_1_database_1_1_d_b_user_manager =
[
    [ "LoginFailException", "class_model_1_1_database_1_1_d_b_user_manager_1_1_login_fail_exception.html", null ],
    [ "UserAlreadyExistsException", "class_model_1_1_database_1_1_d_b_user_manager_1_1_user_already_exists_exception.html", null ],
    [ "DBUserManager", "class_model_1_1_database_1_1_d_b_user_manager.html#a1f92d41f65be57842cd7886e5fc23c5a", null ],
    [ "Login", "class_model_1_1_database_1_1_d_b_user_manager.html#acf6eccfbf05ae841498399c2a7261407", null ],
    [ "Register", "class_model_1_1_database_1_1_d_b_user_manager.html#ae574dfd368c314c032f35837abe50dd5", null ],
    [ "SelectUserContacts", "class_model_1_1_database_1_1_d_b_user_manager.html#a59e1a8a6ad4bb96f6810af4b6e3880cc", null ],
    [ "SelectUserConversations", "class_model_1_1_database_1_1_d_b_user_manager.html#a3b38332e85b2c082d4b1b27462ffb046", null ]
];