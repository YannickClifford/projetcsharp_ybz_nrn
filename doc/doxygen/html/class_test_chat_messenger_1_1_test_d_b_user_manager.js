var class_test_chat_messenger_1_1_test_d_b_user_manager =
[
    [ "CleanUp", "class_test_chat_messenger_1_1_test_d_b_user_manager.html#a3f96bc9df6c7c257c0833d3e50ed2d2f", null ],
    [ "Login_EmailNotFound_Exception", "class_test_chat_messenger_1_1_test_d_b_user_manager.html#a903e2332ac4749705aacb23433fe4907", null ],
    [ "Login_PasswordNotMatch_Exception", "class_test_chat_messenger_1_1_test_d_b_user_manager.html#a28159be851c67d796f53512d7fc5968a", null ],
    [ "Login_RightInformation_Success", "class_test_chat_messenger_1_1_test_d_b_user_manager.html#ac0082ee405ef08b8395a2e8532c16165", null ],
    [ "Register_LoginAfter_Success", "class_test_chat_messenger_1_1_test_d_b_user_manager.html#a33566f6093de123a338cf58d100a2311", null ],
    [ "Register_UserAlreadyExists_Exception", "class_test_chat_messenger_1_1_test_d_b_user_manager.html#aa6055840802e498ebd84da6f3e326b35", null ],
    [ "SelectUserContacts_BasicCase_Success", "class_test_chat_messenger_1_1_test_d_b_user_manager.html#a7cffeac6d537bb0b33b818ba5d43993e", null ],
    [ "SelectUserContacts_UserDoesNotHaveContact_Success", "class_test_chat_messenger_1_1_test_d_b_user_manager.html#adf6bd1aa9549590daaadeef6a421fd66", null ],
    [ "SelectUserConversations_BasicCase_Success", "class_test_chat_messenger_1_1_test_d_b_user_manager.html#af0e24e9f66034b68622f94207369deb4", null ],
    [ "SelectUserConversations_UserDoesNotHaveConversations_Success", "class_test_chat_messenger_1_1_test_d_b_user_manager.html#a96666a89fde6c7a6d2359a484c834418", null ]
];