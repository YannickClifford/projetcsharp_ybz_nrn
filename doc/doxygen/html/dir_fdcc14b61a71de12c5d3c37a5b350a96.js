var dir_fdcc14b61a71de12c5d3c37a5b350a96 =
[
    [ "Entity", "dir_870e27ddf04a3c354e60a3b9623754b4.html", "dir_870e27ddf04a3c354e60a3b9623754b4" ],
    [ "DatabaseConnector.cs", "_database_connector_8cs.html", [
      [ "DatabaseConnector", "class_model_1_1_database_1_1_database_connector.html", "class_model_1_1_database_1_1_database_connector" ]
    ] ],
    [ "DBContactManager.cs", "_d_b_contact_manager_8cs.html", [
      [ "DBContactManager", "class_model_1_1_database_1_1_d_b_contact_manager.html", "class_model_1_1_database_1_1_d_b_contact_manager" ],
      [ "EmailContactDoesNotMatchException", "class_model_1_1_database_1_1_d_b_contact_manager_1_1_email_contact_does_not_match_exception.html", null ],
      [ "NicknameAlreadyUsedException", "class_model_1_1_database_1_1_d_b_contact_manager_1_1_nickname_already_used_exception.html", null ],
      [ "EmailAlreadyUsedException", "class_model_1_1_database_1_1_d_b_contact_manager_1_1_email_already_used_exception.html", null ],
      [ "UserAddingHimselfAsContactException", "class_model_1_1_database_1_1_d_b_contact_manager_1_1_user_adding_himself_as_contact_exception.html", null ]
    ] ],
    [ "DBConversationManager.cs", "_d_b_conversation_manager_8cs.html", [
      [ "DBConversationManager", "class_model_1_1_database_1_1_d_b_conversation_manager.html", "class_model_1_1_database_1_1_d_b_conversation_manager" ],
      [ "ConversationAlreadyExistsException", "class_model_1_1_database_1_1_d_b_conversation_manager_1_1_conversation_already_exists_exception.html", null ],
      [ "OtherParticipantWithoutIdException", "class_model_1_1_database_1_1_d_b_conversation_manager_1_1_other_participant_without_id_exception.html", null ]
    ] ],
    [ "DBMessageManager.cs", "_d_b_message_manager_8cs.html", [
      [ "DBMessageManager", "class_model_1_1_database_1_1_d_b_message_manager.html", "class_model_1_1_database_1_1_d_b_message_manager" ],
      [ "MessageCannotBeAddedException", "class_model_1_1_database_1_1_d_b_message_manager_1_1_message_cannot_be_added_exception.html", null ]
    ] ],
    [ "DBUserManager.cs", "_d_b_user_manager_8cs.html", "_d_b_user_manager_8cs" ]
];