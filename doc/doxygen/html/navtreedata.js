/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Chat Messenger", "index.html", [
    [ "The Bouncy Castle Crypto Package For C Sharp", "md__d_1__projets__chat_messenger_app_packages__bouncy_castle_81_88_83_81__r_e_a_d_m_e.html", [
      [ "Mailing Lists", "md__d_1__projets__chat_messenger_app_packages__bouncy_castle_81_88_83_81__r_e_a_d_m_e.html#autotoc_md1", null ],
      [ "Feedback", "md__d_1__projets__chat_messenger_app_packages__bouncy_castle_81_88_83_81__r_e_a_d_m_e.html#autotoc_md2", null ],
      [ "Finally", "md__d_1__projets__chat_messenger_app_packages__bouncy_castle_81_88_83_81__r_e_a_d_m_e.html#autotoc_md3", null ]
    ] ],
    [ "The Bouncy Castle Crypto Package For C Sharp", "md__d_1__projets__chat_messenger_app_packages__bouncy_castle_81_88_85__r_e_a_d_m_e.html", [
      [ "Mailing Lists", "md__d_1__projets__chat_messenger_app_packages__bouncy_castle_81_88_85__r_e_a_d_m_e.html#autotoc_md5", null ],
      [ "Feedback", "md__d_1__projets__chat_messenger_app_packages__bouncy_castle_81_88_85__r_e_a_d_m_e.html#autotoc_md6", null ],
      [ "Finally", "md__d_1__projets__chat_messenger_app_packages__bouncy_castle_81_88_85__r_e_a_d_m_e.html#autotoc_md7", null ]
    ] ],
    [ "LICENSE", "md__d_1__projets__chat_messenger_app_packages__newtonsoft_8_json_812_80_83__l_i_c_e_n_s_e.html", null ],
    [ "Chat Messenger", "md__d_1__projets__chat_messenger_app__r_e_a_d_m_e.html", [
      [ "Utilsation", "md__d_1__projets__chat_messenger_app__r_e_a_d_m_e.html#autotoc_md9", null ],
      [ "Dossiers", "md__d_1__projets__chat_messenger_app__r_e_a_d_m_e.html#autotoc_md10", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Properties", "functions_prop.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Typedefs", "globals_type.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_b_crypt_8cs.html",
"class_test_chat_messenger_1_1_test_message.html#ac20c772ae035954c47747738ce8f3c50"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';