var namespace_model_1_1_database_1_1_entity =
[
    [ "Contact", "class_model_1_1_database_1_1_entity_1_1_contact.html", "class_model_1_1_database_1_1_entity_1_1_contact" ],
    [ "Conversation", "class_model_1_1_database_1_1_entity_1_1_conversation.html", "class_model_1_1_database_1_1_entity_1_1_conversation" ],
    [ "Message", "class_model_1_1_database_1_1_entity_1_1_message.html", "class_model_1_1_database_1_1_entity_1_1_message" ],
    [ "User", "class_model_1_1_database_1_1_entity_1_1_user.html", "class_model_1_1_database_1_1_entity_1_1_user" ]
];