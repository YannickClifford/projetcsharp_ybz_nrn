var namespace_chat_messenger_g_u_i =
[
    [ "frmAddContact", "class_chat_messenger_g_u_i_1_1frm_add_contact.html", "class_chat_messenger_g_u_i_1_1frm_add_contact" ],
    [ "frmCreateConversation", "class_chat_messenger_g_u_i_1_1frm_create_conversation.html", "class_chat_messenger_g_u_i_1_1frm_create_conversation" ],
    [ "frmMain", "class_chat_messenger_g_u_i_1_1frm_main.html", "class_chat_messenger_g_u_i_1_1frm_main" ],
    [ "frmSign", "class_chat_messenger_g_u_i_1_1frm_sign.html", "class_chat_messenger_g_u_i_1_1frm_sign" ],
    [ "Program", "class_chat_messenger_g_u_i_1_1_program.html", null ],
    [ "ucBubble", "class_chat_messenger_g_u_i_1_1uc_bubble.html", "class_chat_messenger_g_u_i_1_1uc_bubble" ],
    [ "ucConversation", "class_chat_messenger_g_u_i_1_1uc_conversation.html", "class_chat_messenger_g_u_i_1_1uc_conversation" ],
    [ "ucLogin", "class_chat_messenger_g_u_i_1_1uc_login.html", "class_chat_messenger_g_u_i_1_1uc_login" ],
    [ "ucMain", "class_chat_messenger_g_u_i_1_1uc_main.html", "class_chat_messenger_g_u_i_1_1uc_main" ],
    [ "ucRegister", "class_chat_messenger_g_u_i_1_1uc_register.html", "class_chat_messenger_g_u_i_1_1uc_register" ],
    [ "ucValidate", "class_chat_messenger_g_u_i_1_1uc_validate.html", "class_chat_messenger_g_u_i_1_1uc_validate" ]
];