var class_model_1_1_settings =
[
    [ "FromJson", "class_model_1_1_settings.html#a10ca16c3ccd8d27f561f40d6ae3a4d68", null ],
    [ "ToJson", "class_model_1_1_settings.html#a9a3fbdb3676eaa62b4b6baaa1b06aa8b", null ],
    [ "Instance", "class_model_1_1_settings.html#a448a124df5504ba4201511483c8db580", null ],
    [ "DbConnectionString", "class_model_1_1_settings.html#a269bdaa9c5f9bd25973c36fdf6b93713", null ],
    [ "Language", "class_model_1_1_settings.html#a1228a5b9100ca45488132b944dcbfa70", null ],
    [ "UserEmail", "class_model_1_1_settings.html#a49c35b073b16220389f341694fbd9e46", null ],
    [ "WindowsX", "class_model_1_1_settings.html#a75a7e1107932103f6c9c78e05b372f10", null ],
    [ "WindowsY", "class_model_1_1_settings.html#a4120ccaac819110455ecbeaa5ce8189b", null ]
];