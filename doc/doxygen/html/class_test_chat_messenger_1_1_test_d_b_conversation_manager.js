var class_test_chat_messenger_1_1_test_d_b_conversation_manager =
[
    [ "CleanUp", "class_test_chat_messenger_1_1_test_d_b_conversation_manager.html#a5f154a09321eff975130cdd53e2a8c30", null ],
    [ "CreateConversation_ConversationAlreadyExists_Exception", "class_test_chat_messenger_1_1_test_d_b_conversation_manager.html#a39e9f83da36c163e4ce129bcf987b145", null ],
    [ "CreateConversation_OtherParticipantWithoutId_Exception", "class_test_chat_messenger_1_1_test_d_b_conversation_manager.html#ad3cd1c701eab963fa4705b64e12b1866", null ],
    [ "CreateConversation_RightInformation_Success", "class_test_chat_messenger_1_1_test_d_b_conversation_manager.html#a5741ddcc873a90e04210d308103a38f3", null ],
    [ "DeleteConversation_ConversationWithMessagesInDTB_Success", "class_test_chat_messenger_1_1_test_d_b_conversation_manager.html#a324d9a44b52922260e6c2f20f90932f5", null ],
    [ "DeleteConversation_EmptyConversation_Success", "class_test_chat_messenger_1_1_test_d_b_conversation_manager.html#aa03cc166b26beb3ae7771fb722f0e49c", null ],
    [ "Init", "class_test_chat_messenger_1_1_test_d_b_conversation_manager.html#a21e59ed741664d4ff38c9e09b3181b0f", null ],
    [ "SelectConversationMessages_BasicCase_Success", "class_test_chat_messenger_1_1_test_d_b_conversation_manager.html#ad4d49d840ddc6127c727508879a0aa4c", null ],
    [ "UpdateModificationDate_BasicCase_Success", "class_test_chat_messenger_1_1_test_d_b_conversation_manager.html#a8bf05762218eb172cd92b79450b43b9c", null ]
];