var hierarchy =
[
    [ "BCrypt.Net.BCrypt", "class_b_crypt_1_1_net_1_1_b_crypt.html", null ],
    [ "Model.Database.Entity.Contact", "class_model_1_1_database_1_1_entity_1_1_contact.html", null ],
    [ "Model.Database.Entity.Conversation", "class_model_1_1_database_1_1_entity_1_1_conversation.html", null ],
    [ "Model.Database.DatabaseConnector", "class_model_1_1_database_1_1_database_connector.html", [
      [ "Model.Database.DBContactManager", "class_model_1_1_database_1_1_d_b_contact_manager.html", null ],
      [ "Model.Database.DBConversationManager", "class_model_1_1_database_1_1_d_b_conversation_manager.html", null ],
      [ "Model.Database.DBMessageManager", "class_model_1_1_database_1_1_d_b_message_manager.html", null ],
      [ "Model.Database.DBUserManager", "class_model_1_1_database_1_1_d_b_user_manager.html", null ]
    ] ],
    [ "Exception", null, [
      [ "BCrypt.Net.BcryptAuthenticationException", "class_b_crypt_1_1_net_1_1_bcrypt_authentication_exception.html", null ],
      [ "BCrypt.Net.HashInformationException", "class_b_crypt_1_1_net_1_1_hash_information_exception.html", null ],
      [ "BCrypt.Net.SaltParseException", "class_b_crypt_1_1_net_1_1_salt_parse_exception.html", null ],
      [ "Model.Database.DBContactManager.EmailAlreadyUsedException", "class_model_1_1_database_1_1_d_b_contact_manager_1_1_email_already_used_exception.html", null ],
      [ "Model.Database.DBContactManager.EmailContactDoesNotMatchException", "class_model_1_1_database_1_1_d_b_contact_manager_1_1_email_contact_does_not_match_exception.html", null ],
      [ "Model.Database.DBContactManager.NicknameAlreadyUsedException", "class_model_1_1_database_1_1_d_b_contact_manager_1_1_nickname_already_used_exception.html", null ],
      [ "Model.Database.DBContactManager.UserAddingHimselfAsContactException", "class_model_1_1_database_1_1_d_b_contact_manager_1_1_user_adding_himself_as_contact_exception.html", null ],
      [ "Model.Database.DBConversationManager.ConversationAlreadyExistsException", "class_model_1_1_database_1_1_d_b_conversation_manager_1_1_conversation_already_exists_exception.html", null ],
      [ "Model.Database.DBConversationManager.OtherParticipantWithoutIdException", "class_model_1_1_database_1_1_d_b_conversation_manager_1_1_other_participant_without_id_exception.html", null ],
      [ "Model.Database.DBMessageManager.MessageCannotBeAddedException", "class_model_1_1_database_1_1_d_b_message_manager_1_1_message_cannot_be_added_exception.html", null ],
      [ "Model.Database.DBUserManager.LoginFailException", "class_model_1_1_database_1_1_d_b_user_manager_1_1_login_fail_exception.html", null ],
      [ "Model.Database.DBUserManager.UserAlreadyExistsException", "class_model_1_1_database_1_1_d_b_user_manager_1_1_user_already_exists_exception.html", null ]
    ] ],
    [ "BCrypt.Net.HashInformation", "class_b_crypt_1_1_net_1_1_hash_information.html", null ],
    [ "Model.Database.Entity.Message", "class_model_1_1_database_1_1_entity_1_1_message.html", null ],
    [ "MetroForm", null, [
      [ "ChatMessengerGUI.frmAddContact", "class_chat_messenger_g_u_i_1_1frm_add_contact.html", null ],
      [ "ChatMessengerGUI.frmCreateConversation", "class_chat_messenger_g_u_i_1_1frm_create_conversation.html", null ],
      [ "ChatMessengerGUI.frmMain", "class_chat_messenger_g_u_i_1_1frm_main.html", null ],
      [ "ChatMessengerGUI.frmSign", "class_chat_messenger_g_u_i_1_1frm_sign.html", null ]
    ] ],
    [ "MetroUserControl", null, [
      [ "ChatMessengerGUI.ucLogin", "class_chat_messenger_g_u_i_1_1uc_login.html", null ],
      [ "ChatMessengerGUI.ucRegister", "class_chat_messenger_g_u_i_1_1uc_register.html", null ],
      [ "ChatMessengerGUI.ucValidate", "class_chat_messenger_g_u_i_1_1uc_validate.html", null ]
    ] ],
    [ "Model.PasswordCheck", "class_model_1_1_password_check.html", null ],
    [ "ChatMessengerGUI.Program", "class_chat_messenger_g_u_i_1_1_program.html", null ],
    [ "Model.Settings", "class_model_1_1_settings.html", null ],
    [ "TestChatMessenger.TestContact", "class_test_chat_messenger_1_1_test_contact.html", null ],
    [ "TestChatMessenger.TestConversation", "class_test_chat_messenger_1_1_test_conversation.html", null ],
    [ "TestChatMessenger.TestDBContactManager", "class_test_chat_messenger_1_1_test_d_b_contact_manager.html", null ],
    [ "TestChatMessenger.TestDBConversationManager", "class_test_chat_messenger_1_1_test_d_b_conversation_manager.html", null ],
    [ "TestChatMessenger.TestDBMessageManager", "class_test_chat_messenger_1_1_test_d_b_message_manager.html", null ],
    [ "TestChatMessenger.TestDBUserManager", "class_test_chat_messenger_1_1_test_d_b_user_manager.html", null ],
    [ "TestChatMessenger.TestMessage", "class_test_chat_messenger_1_1_test_message.html", null ],
    [ "TestChatMessenger.TestPasswordCheck", "class_test_chat_messenger_1_1_test_password_check.html", null ],
    [ "TestChatMessenger.TestSettings", "class_test_chat_messenger_1_1_test_settings.html", null ],
    [ "TestChatMessenger.TestUser", "class_test_chat_messenger_1_1_test_user.html", null ],
    [ "Model.Database.Entity.User", "class_model_1_1_database_1_1_entity_1_1_user.html", null ],
    [ "UserControl", null, [
      [ "ChatMessengerGUI.ucBubble", "class_chat_messenger_g_u_i_1_1uc_bubble.html", null ],
      [ "ChatMessengerGUI.ucConversation", "class_chat_messenger_g_u_i_1_1uc_conversation.html", null ],
      [ "ChatMessengerGUI.ucMain", "class_chat_messenger_g_u_i_1_1uc_main.html", null ]
    ] ]
];