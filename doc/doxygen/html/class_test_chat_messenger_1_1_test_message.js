var class_test_chat_messenger_1_1_test_message =
[
    [ "CleanUp", "class_test_chat_messenger_1_1_test_message.html#ac15533e68a01f9f6fb78a7f3dc95f30b", null ],
    [ "Id_LessThan1_Exception", "class_test_chat_messenger_1_1_test_message.html#a8ae82ec772bdd0e851170b3ebd1836a6", null ],
    [ "Init", "class_test_chat_messenger_1_1_test_message.html#a4fddaa24b9173eaa3226afc2aafa4479", null ],
    [ "NewMessage_ContentEmpty_Exception", "class_test_chat_messenger_1_1_test_message.html#a264c002541e0a8cfba3e75bedf6c9bf4", null ],
    [ "NewMessage_ContentNull_Exception", "class_test_chat_messenger_1_1_test_message.html#a7a2194c232f5efd83e86b9d331a67b34", null ],
    [ "NewMessage_ContentWithWhiteSpacesOnly_Exception", "class_test_chat_messenger_1_1_test_message.html#a7f61f9b83146ffd29c52fe739c9b501c", null ],
    [ "NewMessage_PropertiesAfterCreation_Success", "class_test_chat_messenger_1_1_test_message.html#adf85dbf9472ea319efee97bf967cb1e0", null ],
    [ "NewMessage_SenderIdLessThan1_Exception", "class_test_chat_messenger_1_1_test_message.html#ac20c772ae035954c47747738ce8f3c50", null ]
];