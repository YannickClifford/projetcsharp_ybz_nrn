var class_model_1_1_database_1_1_d_b_message_manager =
[
    [ "MessageCannotBeAddedException", "class_model_1_1_database_1_1_d_b_message_manager_1_1_message_cannot_be_added_exception.html", null ],
    [ "DBMessageManager", "class_model_1_1_database_1_1_d_b_message_manager.html#aba9f0cbceeeb61a5573cfc2eb1a7f242", null ],
    [ "AddMessage", "class_model_1_1_database_1_1_d_b_message_manager.html#a18ee18f350c0a29d03edb08db4c2a36f", null ],
    [ "DeleteMessage", "class_model_1_1_database_1_1_d_b_message_manager.html#a4b0c6c2fa1b1bde44520a36f129ef903", null ]
];