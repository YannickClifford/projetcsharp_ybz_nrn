var class_model_1_1_database_1_1_d_b_contact_manager =
[
    [ "EmailAlreadyUsedException", "class_model_1_1_database_1_1_d_b_contact_manager_1_1_email_already_used_exception.html", null ],
    [ "EmailContactDoesNotMatchException", "class_model_1_1_database_1_1_d_b_contact_manager_1_1_email_contact_does_not_match_exception.html", null ],
    [ "NicknameAlreadyUsedException", "class_model_1_1_database_1_1_d_b_contact_manager_1_1_nickname_already_used_exception.html", null ],
    [ "UserAddingHimselfAsContactException", "class_model_1_1_database_1_1_d_b_contact_manager_1_1_user_adding_himself_as_contact_exception.html", null ],
    [ "DBContactManager", "class_model_1_1_database_1_1_d_b_contact_manager.html#abd54aabdea6499e873876da53ab9026b", null ],
    [ "AddContact", "class_model_1_1_database_1_1_d_b_contact_manager.html#a7be5a52649f1ab028b939d950b2b9585", null ]
];