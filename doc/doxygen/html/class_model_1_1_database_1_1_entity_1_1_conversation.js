var class_model_1_1_database_1_1_entity_1_1_conversation =
[
    [ "Conversation", "class_model_1_1_database_1_1_entity_1_1_conversation.html#a52d922af579300109e59b017a190c006", null ],
    [ "CreationDate", "class_model_1_1_database_1_1_entity_1_1_conversation.html#a71dcf83800258c140855822fcf994801", null ],
    [ "Id", "class_model_1_1_database_1_1_entity_1_1_conversation.html#a23a705b9b2a1757d6b56f0990b973667", null ],
    [ "Messages", "class_model_1_1_database_1_1_entity_1_1_conversation.html#ae5953433851ca960a675bde0336d9248", null ],
    [ "OtherParticipant", "class_model_1_1_database_1_1_entity_1_1_conversation.html#aefa4c76ec72a145c17d5b4575e1b8ae8", null ],
    [ "Owner", "class_model_1_1_database_1_1_entity_1_1_conversation.html#a8ddb81193ea6198007af3eb860ceffd2", null ],
    [ "UpdateDate", "class_model_1_1_database_1_1_entity_1_1_conversation.html#ac09e88030dfa49f55a07e8b288383baf", null ]
];