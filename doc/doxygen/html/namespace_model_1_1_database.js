var namespace_model_1_1_database =
[
    [ "Entity", "namespace_model_1_1_database_1_1_entity.html", "namespace_model_1_1_database_1_1_entity" ],
    [ "DatabaseConnector", "class_model_1_1_database_1_1_database_connector.html", "class_model_1_1_database_1_1_database_connector" ],
    [ "DBContactManager", "class_model_1_1_database_1_1_d_b_contact_manager.html", "class_model_1_1_database_1_1_d_b_contact_manager" ],
    [ "DBConversationManager", "class_model_1_1_database_1_1_d_b_conversation_manager.html", "class_model_1_1_database_1_1_d_b_conversation_manager" ],
    [ "DBMessageManager", "class_model_1_1_database_1_1_d_b_message_manager.html", "class_model_1_1_database_1_1_d_b_message_manager" ],
    [ "DBUserManager", "class_model_1_1_database_1_1_d_b_user_manager.html", "class_model_1_1_database_1_1_d_b_user_manager" ]
];