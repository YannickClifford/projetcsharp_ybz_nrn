var class_test_chat_messenger_1_1_test_d_b_contact_manager =
[
    [ "AddContact_EmailAlreadyUsed_Exception", "class_test_chat_messenger_1_1_test_d_b_contact_manager.html#aca42ffd6c0915fa4e551170ee57aa408", null ],
    [ "AddContact_EmailContactDoesNotMatch_Exception", "class_test_chat_messenger_1_1_test_d_b_contact_manager.html#ab98506ae496588ccf6d22b3ef55723f1", null ],
    [ "AddContact_NicknameAlreadyUsedAndEmailDoesNotMatch_Exception", "class_test_chat_messenger_1_1_test_d_b_contact_manager.html#a76fcb960b006645d62b868bbfc35e6dd", null ],
    [ "AddContact_NicknameAlreadyUsedAndEmailMatches_Exception", "class_test_chat_messenger_1_1_test_d_b_contact_manager.html#a6b842aaad6f2d168ef62871c6a4e09a1", null ],
    [ "AddContact_RightInformation_Success", "class_test_chat_messenger_1_1_test_d_b_contact_manager.html#a32d0f017a96f363f9c11ff6b2631aba6", null ],
    [ "AddContact_UserAddingHimselfAsContact_Exception", "class_test_chat_messenger_1_1_test_d_b_contact_manager.html#a021bc3ae0413114c17bd016a379d9b9f", null ],
    [ "CleanUp", "class_test_chat_messenger_1_1_test_d_b_contact_manager.html#a8ff2a64805c2e688071c935b924bd8ee", null ],
    [ "Init", "class_test_chat_messenger_1_1_test_d_b_contact_manager.html#a1228dd11c10ce1201132eede817d640a", null ]
];