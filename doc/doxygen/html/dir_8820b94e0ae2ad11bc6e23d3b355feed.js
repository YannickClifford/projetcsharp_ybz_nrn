var dir_8820b94e0ae2ad11bc6e23d3b355feed =
[
    [ "Properties", "dir_a793036b4d2a8d44ff92a621f3da2588.html", "dir_a793036b4d2a8d44ff92a621f3da2588" ],
    [ "frmAddContact.cs", "frm_add_contact_8cs.html", [
      [ "frmAddContact", "class_chat_messenger_g_u_i_1_1frm_add_contact.html", "class_chat_messenger_g_u_i_1_1frm_add_contact" ]
    ] ],
    [ "frmAddContact.Designer.cs", "frm_add_contact_8_designer_8cs.html", [
      [ "frmAddContact", "class_chat_messenger_g_u_i_1_1frm_add_contact.html", "class_chat_messenger_g_u_i_1_1frm_add_contact" ]
    ] ],
    [ "frmCreateConversation.cs", "frm_create_conversation_8cs.html", [
      [ "frmCreateConversation", "class_chat_messenger_g_u_i_1_1frm_create_conversation.html", "class_chat_messenger_g_u_i_1_1frm_create_conversation" ]
    ] ],
    [ "frmCreateConversation.Designer.cs", "frm_create_conversation_8_designer_8cs.html", [
      [ "frmCreateConversation", "class_chat_messenger_g_u_i_1_1frm_create_conversation.html", "class_chat_messenger_g_u_i_1_1frm_create_conversation" ]
    ] ],
    [ "frmMain.cs", "frm_main_8cs.html", [
      [ "frmMain", "class_chat_messenger_g_u_i_1_1frm_main.html", "class_chat_messenger_g_u_i_1_1frm_main" ]
    ] ],
    [ "frmMain.Designer.cs", "frm_main_8_designer_8cs.html", [
      [ "frmMain", "class_chat_messenger_g_u_i_1_1frm_main.html", "class_chat_messenger_g_u_i_1_1frm_main" ]
    ] ],
    [ "frmSign.cs", "frm_sign_8cs.html", [
      [ "frmSign", "class_chat_messenger_g_u_i_1_1frm_sign.html", "class_chat_messenger_g_u_i_1_1frm_sign" ]
    ] ],
    [ "frmSign.Designer.cs", "frm_sign_8_designer_8cs.html", [
      [ "frmSign", "class_chat_messenger_g_u_i_1_1frm_sign.html", "class_chat_messenger_g_u_i_1_1frm_sign" ]
    ] ],
    [ "Program.cs", "_program_8cs.html", [
      [ "Program", "class_chat_messenger_g_u_i_1_1_program.html", null ]
    ] ],
    [ "ucBubble.cs", "uc_bubble_8cs.html", "uc_bubble_8cs" ],
    [ "ucBubble.Designer.cs", "uc_bubble_8_designer_8cs.html", [
      [ "ucBubble", "class_chat_messenger_g_u_i_1_1uc_bubble.html", "class_chat_messenger_g_u_i_1_1uc_bubble" ]
    ] ],
    [ "ucConversation.cs", "uc_conversation_8cs.html", "uc_conversation_8cs" ],
    [ "ucConversation.Designer.cs", "uc_conversation_8_designer_8cs.html", [
      [ "ucConversation", "class_chat_messenger_g_u_i_1_1uc_conversation.html", "class_chat_messenger_g_u_i_1_1uc_conversation" ]
    ] ],
    [ "ucLogin.cs", "uc_login_8cs.html", [
      [ "ucLogin", "class_chat_messenger_g_u_i_1_1uc_login.html", "class_chat_messenger_g_u_i_1_1uc_login" ]
    ] ],
    [ "ucLogin.Designer.cs", "uc_login_8_designer_8cs.html", [
      [ "ucLogin", "class_chat_messenger_g_u_i_1_1uc_login.html", "class_chat_messenger_g_u_i_1_1uc_login" ]
    ] ],
    [ "ucMain.cs", "uc_main_8cs.html", [
      [ "ucMain", "class_chat_messenger_g_u_i_1_1uc_main.html", "class_chat_messenger_g_u_i_1_1uc_main" ]
    ] ],
    [ "ucMain.Designer.cs", "uc_main_8_designer_8cs.html", [
      [ "ucMain", "class_chat_messenger_g_u_i_1_1uc_main.html", "class_chat_messenger_g_u_i_1_1uc_main" ]
    ] ],
    [ "ucRegister.cs", "uc_register_8cs.html", [
      [ "ucRegister", "class_chat_messenger_g_u_i_1_1uc_register.html", "class_chat_messenger_g_u_i_1_1uc_register" ]
    ] ],
    [ "ucRegister.Designer.cs", "uc_register_8_designer_8cs.html", [
      [ "ucRegister", "class_chat_messenger_g_u_i_1_1uc_register.html", "class_chat_messenger_g_u_i_1_1uc_register" ]
    ] ],
    [ "ucValidate.cs", "uc_validate_8cs.html", [
      [ "ucValidate", "class_chat_messenger_g_u_i_1_1uc_validate.html", "class_chat_messenger_g_u_i_1_1uc_validate" ]
    ] ],
    [ "ucValidate.Designer.cs", "uc_validate_8_designer_8cs.html", [
      [ "ucValidate", "class_chat_messenger_g_u_i_1_1uc_validate.html", "class_chat_messenger_g_u_i_1_1uc_validate" ]
    ] ]
];