var class_model_1_1_database_1_1_d_b_conversation_manager =
[
    [ "ConversationAlreadyExistsException", "class_model_1_1_database_1_1_d_b_conversation_manager_1_1_conversation_already_exists_exception.html", null ],
    [ "OtherParticipantWithoutIdException", "class_model_1_1_database_1_1_d_b_conversation_manager_1_1_other_participant_without_id_exception.html", null ],
    [ "DBConversationManager", "class_model_1_1_database_1_1_d_b_conversation_manager.html#a612514896d0f5d2d3ce2b91f890f8cf4", null ],
    [ "CreateConversation", "class_model_1_1_database_1_1_d_b_conversation_manager.html#abf2ef44a60617469f1237e10c57c92c1", null ],
    [ "DeleteConversation", "class_model_1_1_database_1_1_d_b_conversation_manager.html#ac741e6e5e399ad11c69ccd9d4206c45b", null ],
    [ "SelectConversationMessages", "class_model_1_1_database_1_1_d_b_conversation_manager.html#a9317688770ce02406de236d0170c6c22", null ],
    [ "UpdateModificationDate", "class_model_1_1_database_1_1_d_b_conversation_manager.html#a132a72e821c7e196c967c701a98e1072", null ]
];