var dir_a65b763d428659d117f335f4fb602db2 =
[
    [ "Properties", "dir_f9da18d7e9c9063b191d4d833b196d46.html", "dir_f9da18d7e9c9063b191d4d833b196d46" ],
    [ "TestContact.cs", "_test_contact_8cs.html", [
      [ "TestContact", "class_test_chat_messenger_1_1_test_contact.html", "class_test_chat_messenger_1_1_test_contact" ]
    ] ],
    [ "TestConversation.cs", "_test_conversation_8cs.html", [
      [ "TestConversation", "class_test_chat_messenger_1_1_test_conversation.html", "class_test_chat_messenger_1_1_test_conversation" ]
    ] ],
    [ "TestDBContactManager.cs", "_test_d_b_contact_manager_8cs.html", [
      [ "TestDBContactManager", "class_test_chat_messenger_1_1_test_d_b_contact_manager.html", "class_test_chat_messenger_1_1_test_d_b_contact_manager" ]
    ] ],
    [ "TestDBConversationManager.cs", "_test_d_b_conversation_manager_8cs.html", [
      [ "TestDBConversationManager", "class_test_chat_messenger_1_1_test_d_b_conversation_manager.html", "class_test_chat_messenger_1_1_test_d_b_conversation_manager" ]
    ] ],
    [ "TestDBMessageManager.cs", "_test_d_b_message_manager_8cs.html", [
      [ "TestDBMessageManager", "class_test_chat_messenger_1_1_test_d_b_message_manager.html", "class_test_chat_messenger_1_1_test_d_b_message_manager" ]
    ] ],
    [ "TestDBUserManager.cs", "_test_d_b_user_manager_8cs.html", [
      [ "TestDBUserManager", "class_test_chat_messenger_1_1_test_d_b_user_manager.html", "class_test_chat_messenger_1_1_test_d_b_user_manager" ]
    ] ],
    [ "TestMessage.cs", "_test_message_8cs.html", [
      [ "TestMessage", "class_test_chat_messenger_1_1_test_message.html", "class_test_chat_messenger_1_1_test_message" ]
    ] ],
    [ "TestPasswordCheck.cs", "_test_password_check_8cs.html", [
      [ "TestPasswordCheck", "class_test_chat_messenger_1_1_test_password_check.html", "class_test_chat_messenger_1_1_test_password_check" ]
    ] ],
    [ "TestSettings.cs", "_test_settings_8cs.html", [
      [ "TestSettings", "class_test_chat_messenger_1_1_test_settings.html", "class_test_chat_messenger_1_1_test_settings" ]
    ] ],
    [ "TestUser.cs", "_test_user_8cs.html", [
      [ "TestUser", "class_test_chat_messenger_1_1_test_user.html", "class_test_chat_messenger_1_1_test_user" ]
    ] ]
];