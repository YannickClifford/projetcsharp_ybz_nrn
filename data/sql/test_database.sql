/*
 * Author		: Yannick.BAUDRAZ@cpnv.ch
 * Version		: 16 DEC 2019
 * Description	: Create the database `test_ybznrn` for unit tests of `chat_messenger`
 *               Make sur to execute this script before run units tests
 */

/* Database */
DROP DATABASE IF EXISTS `test_ybznrn`;

CREATE DATABASE `test_ybznrn`;

USE `test_ybznrn`;

/** Users */
-- Create table
Create TABLE `users` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `email` VARCHAR(320) NOT NULL,
    `password` VARCHAR(60) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE (`email`)
) ENGINE = INNODB;

-- Insert some users
INSERT INTO
    users (email, password)
VALUES
    ('test@cpnv.ch', '$2a$11$IrDEvwmT1bG5bRyThWZlm.bhIivpvZYROGS9tTU9YEyNPTnKfxb2e'), -- qwe123!$
    ('yannick.baudraz@cpnv.ch', '$2a$11$ov08kIh6fWi.S6OFnAL0le119lEQl1ikuXruugcsul5sRX2nK1n.C'), -- Pa$$w0rd
    ('nathan.rayburn@cpnv.ch', '$2a$11$NWNbc29W.XJNfKh2y3SWeemfPvERLFo.GTtXNR2tDLDxZDed1E0qC'), -- W0rdPa$$
    ('tester@gmail.com', '$2a$11$NWNbc29W.XJNfKh2y3SWeemfPvERLFo.GTtXNR2tDLDxZDed1E0qC'), -- W0rdPa$$
    ('userhavecontact@hotmail.com', '$2a$11$ov08kIh6fWi.S6OFnAL0le119lEQl1ikuXruugcsul5sRX2nK1n.C'), -- Pa$$w0rd
    ('userwithoutcontact@hotmail.com', '$2a$11$ov08kIh6fWi.S6OFnAL0le119lEQl1ikuXruugcsul5sRX2nK1n.C'),
    ('conversationexists@cpnv.ch', '$2a$11$ov08kIh6fWi.S6OFnAL0le119lEQl1ikuXruugcsul5sRX2nK1n.C'),
    ('conversationRightInfo@cpnv.ch', '$2a$11$ov08kIh6fWi.S6OFnAL0le119lEQl1ikuXruugcsul5sRX2nK1n.C'),
    ('userHaveConversations@cpnv.ch', '$2a$11$ov08kIh6fWi.S6OFnAL0le119lEQl1ikuXruugcsul5sRX2nK1n.C'),
    ('userWithoutConversations@cpnv.ch', '$2a$11$ov08kIh6fWi.S6OFnAL0le119lEQl1ikuXruugcsul5sRX2nK1n.C'), -- 10
	 ('contactWithoutId@cpnv.ch', '$2a$11$ov08kIh6fWi.S6OFnAL0le119lEQl1ikuXruugcsul5sRX2nK1n.C'),
	 ('usernotinconversation', '$2a$11$ov08kIh6fWi.S6OFnAL0le119lEQl1ikuXruugcsul5sRX2nK1n.C'),
	 ('userfordeletemessage', '$2a$11$ov08kIh6fWi.S6OFnAL0le119lEQl1ikuXruugcsul5sRX2nK1n.C'),
	 ('userfordeleteConversation', '$2a$11$ov08kIh6fWi.S6OFnAL0le119lEQl1ikuXruugcsul5sRX2nK1n.C');

/** Users contacts */
-- Create Table
CREATE TABLE `users_contacts` (
    `user_id` INT UNSIGNED NOT NULL,
    `contact_id` INT UNSIGNED NOT NULL,
    `nickname` VARCHAR(30) NOT NULL,
    PRIMARY KEY (`user_id`, `contact_id`),
    INDEX (`contact_id`),
    INDEX (`user_id`),
    UNIQUE (`nickname`, `user_id`),
    CONSTRAINT `users_contacts_chk_users-contacts-differents` CHECK (`user_id` <> `contact_id`),
    CONSTRAINT `fk_users_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `fk_users_contact` FOREIGN KEY (`contact_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = INNODB;

-- Insert some contacts
INSERT INTO
	 users_contacts (user_id, contact_id, nickname)
VALUES
    (1, 2, 'Mr. Robot'),
    (2, 3, 'Nate'),
    (5, 1, 'Tester friend'),
    (5, 2, 'Han Solo'),
    (5, 4, 'Metagross'),
    (2, 7, 'conversationExists'),
    (2, 8, 'Right info'),
    (9, 1, 'Tester'),
    (9, 2, 'Cliff'),
	 (2, 11, 'Contact no id'),
	 (13, 2, 'Yannick'),
	 (14, 2, 'Yannick'),
	 (14, 1, 'Test user');

/** Conversations */
-- Create Table
CREATE TABLE `conversations` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `creation_date` DATETIME NOT NULL DEFAULT now(),
    `update_date` DATETIME NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;

-- Insert some conversations
INSERT INTO
    conversations ()
VALUES
    (),
    (),
    (),
	 (),
	 (), -- 5
	 (),
	 ();

/** conversation_participants */
-- Create Table
CREATE TABLE `conversation_participants` (
	 `conversation_id` INT UNSIGNED NOT NULL,
    `user_id` INT UNSIGNED NOT NULL,
    PRIMARY KEY (`conversation_id`, `user_id`),
    INDEX (`user_id`),
    INDEX (`conversation_id`),
    CONSTRAINT `fk_conversation_participants_conversation` FOREIGN KEY (`conversation_id`) REFERENCES `conversations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `fk_conversation_participants_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = INNODB;

-- Insert some participants
INSERT INTO
	 conversation_participants
VALUES
	 (1, 2),
	 (1, 7),
	 (2, 9),
	 (2, 2),
	 (3, 9),
	 (3, 1),
	 (4, 13),
	 (4, 2),
	 (5, 14),
	 (5, 1),
	 (6, 14),
	 (6, 2);
	 
/** messages */
-- Create Table
CREATE TABLE `messages` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `conversation_id` INT UNSIGNED NOT NULL,
    `user_id` INT UNSIGNED NOT NULL,
    `creation_date` DATETIME NOT NULL DEFAULT now(),
    `content` TEXT NOT NULL,
    PRIMARY KEY (`id`),
    INDEX (`conversation_id`, `user_id`),
    CONSTRAINT `fk_messages_conversation_participants` FOREIGN KEY (`conversation_id`, `user_id`) REFERENCES `conversation_participants` (`conversation_id`, `user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = INNODB;

-- Insert some messages
INSERT INTO
	 messages (conversation_id, user_id, content)
VALUES
	 (2, 2, 'Salut !'),
	 (2, 2, 'Comment vas-tu ?'),
	 (2, 9, 'Très bien et toi ?'),
	 (2, 2, 'Bien, merci !'),
	 (4, 13, 'Je vais être supprimé'),
	 (6, 14, 'Nouveau message'),
	 (6, 2, 'Réponse au message');

/* Admin */
DROP USER IF EXISTS 'test_ybznrn_tester' @'localhost';

CREATE USER 'test_ybznrn_tester' @'localhost' IDENTIFIED BY 'Pa$$w0rd';

GRANT USAGE ON *.* TO 'test_ybznrn_tester' @'localhost';

GRANT ALL ON `test_ybznrn`.* TO 'test_ybznrn_tester' @'localhost' WITH GRANT OPTION;

FLUSH PRIVILEGES;
