/*
 * Author		: Yannick.BAUDRAZ@cpnv.ch
 * Version		: 18 DEC 2019
 * Description	: Create tables for database `hapygest_msg`
*/

/* Database hapygest_msg */
USE `hapygest_msg`;

/* Drop tables */
DROP TABLE IF EXISTS `messages`;
DROP TABLE IF EXISTS `conversation_participants`;
DROP TABLE IF EXISTS `conversations`;
DROP TABLE IF EXISTS `users_contacts`;
DROP TABLE IF EXISTS `users`;

/* Table users */
Create TABLE `users` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`email` VARCHAR(320) NOT NULL,
	`password` VARCHAR(60) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE (`email`)
);

/* users_contacts */
CREATE TABLE `users_contacts` (
    `user_id` INT UNSIGNED NOT NULL,
    `contact_id` INT UNSIGNED NOT NULL,
    `nickname` VARCHAR(30) NOT NULL,
    PRIMARY KEY (`user_id`, `contact_id`),
    INDEX (`contact_id`),
    INDEX (`user_id`),
    UNIQUE (`nickname`, `user_id`),
    CONSTRAINT `users_contacts_chk_users-contacts-differents` CHECK (`user_id` <> `contact_id`),
    CONSTRAINT `fk_users_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `fk_users_contact` FOREIGN KEY (`contact_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = INNODB;

/* conversations */
CREATE TABLE `conversations` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `creation_date` DATETIME NOT NULL DEFAULT now(),
    `update_date` DATETIME NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;

/* conversation_participants */
CREATE TABLE `conversation_participants` (
    `conversation_id` INT UNSIGNED NOT NULL,
    `user_id` INT UNSIGNED NOT NULL,
    PRIMARY KEY (`conversation_id`, `user_id`),
    INDEX (`user_id` ASC),
    INDEX (`conversation_id` ASC),
    CONSTRAINT `fk_conversation_participants_conversation` FOREIGN KEY (`conversation_id`) REFERENCES `conversations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `fk_conversation_participants_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = INNODB;

/* messages */
CREATE TABLE `messages` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `conversation_id` INT UNSIGNED NOT NULL,
    `user_id` INT UNSIGNED NOT NULL,
    `creation_date` DATETIME NOT NULL DEFAULT now(),
    `content` TEXT NOT NULL,
    PRIMARY KEY (`id`),
    INDEX (`conversation_id`, `user_id`),
    CONSTRAINT `fk_messages_conversation_participants` FOREIGN KEY (`conversation_id`, `user_id`) REFERENCES `conversation_participants` (`conversation_id`, `user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = INNODB;
