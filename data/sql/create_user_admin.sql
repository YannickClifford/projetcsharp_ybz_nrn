/*
 * Author		: Yannick.BAUDRAZ@cpnv.ch
 * Version		: 02 DEC 2019
 * Description	: Create admin user for database `chat_messenger`
*/

DROP USER IF EXISTS 'chat_messenger_admin'@'localhost';
CREATE USER 'chat_messenger_admin'@'localhost' IDENTIFIED BY 'kwDr$c5.';
GRANT USAGE ON *.* TO 'chat_messenger_admin'@'localhost';
GRANT ALL ON `chat_messenger`.* TO 'chat_messenger_admin'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;
