/*
 * Author		: Yannick.BAUDRAZ@cpnv.ch
 * Version		: 18 DEC 2019
 * Description	: Create database `hapygest_msg`
*/

DROP DATABASE IF EXISTS `hapygest_msg`;
CREATE DATABASE `hapygest_msg`;
