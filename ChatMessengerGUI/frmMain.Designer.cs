﻿namespace ChatMessengerGUI
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.pnlMain = new MetroFramework.Controls.MetroPanel();
            this.cmdLogout = new System.Windows.Forms.Button();
            this.pnlDecorationHeader2 = new System.Windows.Forms.Panel();
            this.cmdBack = new System.Windows.Forms.Button();
            this.lblUser = new System.Windows.Forms.Label();
            this.pnlDecorationHeader1 = new System.Windows.Forms.Panel();
            this.cmdExit = new System.Windows.Forms.Button();
            this.cmdMinimize = new System.Windows.Forms.Button();
            this.pnlDecorationHeader2.SuspendLayout();
            this.pnlDecorationHeader1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.AutoSize = true;
            this.pnlMain.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlMain.HorizontalScrollbarBarColor = true;
            this.pnlMain.HorizontalScrollbarHighlightOnWheel = false;
            this.pnlMain.HorizontalScrollbarSize = 10;
            this.pnlMain.Location = new System.Drawing.Point(1, 58);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(0, 0);
            this.pnlMain.TabIndex = 1;
            this.pnlMain.VerticalScrollbarBarColor = true;
            this.pnlMain.VerticalScrollbarHighlightOnWheel = false;
            this.pnlMain.VerticalScrollbarSize = 10;
            // 
            // cmdLogout
            // 
            this.cmdLogout.BackColor = System.Drawing.Color.Transparent;
            this.cmdLogout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdLogout.FlatAppearance.BorderSize = 0;
            this.cmdLogout.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed;
            this.cmdLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdLogout.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdLogout.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.cmdLogout.Location = new System.Drawing.Point(644, 2);
            this.cmdLogout.Name = "cmdLogout";
            this.cmdLogout.Size = new System.Drawing.Size(113, 26);
            this.cmdLogout.TabIndex = 5;
            this.cmdLogout.Text = "Logout";
            this.cmdLogout.UseVisualStyleBackColor = false;
            this.cmdLogout.Click += new System.EventHandler(this.cmdLogout_Click);
            // 
            // pnlDecorationHeader2
            // 
            this.pnlDecorationHeader2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(164)))), ((int)(((byte)(147)))));
            this.pnlDecorationHeader2.Controls.Add(this.cmdBack);
            this.pnlDecorationHeader2.Controls.Add(this.lblUser);
            this.pnlDecorationHeader2.Controls.Add(this.cmdLogout);
            this.pnlDecorationHeader2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.pnlDecorationHeader2.Location = new System.Drawing.Point(0, 28);
            this.pnlDecorationHeader2.Name = "pnlDecorationHeader2";
            this.pnlDecorationHeader2.Size = new System.Drawing.Size(824, 30);
            this.pnlDecorationHeader2.TabIndex = 6;
            this.pnlDecorationHeader2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlDecorationHeader2_MouseDown);
            this.pnlDecorationHeader2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnlDecorationHeader2_MouseMove);
            // 
            // cmdBack
            // 
            this.cmdBack.BackColor = System.Drawing.Color.Transparent;
            this.cmdBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdBack.FlatAppearance.BorderSize = 0;
            this.cmdBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed;
            this.cmdBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdBack.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdBack.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.cmdBack.Location = new System.Drawing.Point(20, 4);
            this.cmdBack.Name = "cmdBack";
            this.cmdBack.Size = new System.Drawing.Size(113, 23);
            this.cmdBack.TabIndex = 6;
            this.cmdBack.Text = "Back";
            this.cmdBack.UseVisualStyleBackColor = false;
            this.cmdBack.Visible = false;
            this.cmdBack.Click += new System.EventHandler(this.cmdBack_Click_1);
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblUser.ForeColor = System.Drawing.Color.White;
            this.lblUser.Location = new System.Drawing.Point(387, 7);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(40, 18);
            this.lblUser.TabIndex = 8;
            this.lblUser.Text = "User";
            this.lblUser.Visible = false;
            // 
            // pnlDecorationHeader1
            // 
            this.pnlDecorationHeader1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(140)))), ((int)(((byte)(127)))));
            this.pnlDecorationHeader1.Controls.Add(this.cmdExit);
            this.pnlDecorationHeader1.Controls.Add(this.cmdMinimize);
            this.pnlDecorationHeader1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.pnlDecorationHeader1.Location = new System.Drawing.Point(0, 5);
            this.pnlDecorationHeader1.Name = "pnlDecorationHeader1";
            this.pnlDecorationHeader1.Size = new System.Drawing.Size(824, 34);
            this.pnlDecorationHeader1.TabIndex = 7;
            this.pnlDecorationHeader1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlDecorationHeader1_MouseDown);
            this.pnlDecorationHeader1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnlDecorationHeader1_MouseMove);
            // 
            // cmdExit
            // 
            this.cmdExit.BackColor = System.Drawing.Color.Transparent;
            this.cmdExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdExit.FlatAppearance.BorderSize = 0;
            this.cmdExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed;
            this.cmdExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdExit.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdExit.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.cmdExit.Location = new System.Drawing.Point(729, 3);
            this.cmdExit.Name = "cmdExit";
            this.cmdExit.Size = new System.Drawing.Size(28, 23);
            this.cmdExit.TabIndex = 10;
            this.cmdExit.Text = "X";
            this.cmdExit.UseVisualStyleBackColor = false;
            this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
            // 
            // cmdMinimize
            // 
            this.cmdMinimize.BackColor = System.Drawing.Color.Transparent;
            this.cmdMinimize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMinimize.FlatAppearance.BorderSize = 0;
            this.cmdMinimize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed;
            this.cmdMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMinimize.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdMinimize.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.cmdMinimize.Location = new System.Drawing.Point(706, 3);
            this.cmdMinimize.Name = "cmdMinimize";
            this.cmdMinimize.Size = new System.Drawing.Size(28, 23);
            this.cmdMinimize.TabIndex = 9;
            this.cmdMinimize.Text = "__";
            this.cmdMinimize.UseVisualStyleBackColor = false;
            this.cmdMinimize.Click += new System.EventHandler(this.cmdMinimize_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(770, 437);
            this.Controls.Add(this.pnlDecorationHeader2);
            this.Controls.Add(this.pnlDecorationHeader1);
            this.Controls.Add(this.pnlMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.Resizable = false;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.pnlDecorationHeader2.ResumeLayout(false);
            this.pnlDecorationHeader2.PerformLayout();
            this.pnlDecorationHeader1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button cmdLogout;
        private System.Windows.Forms.Panel pnlDecorationHeader2;
        private System.Windows.Forms.Panel pnlDecorationHeader1;
        private System.Windows.Forms.Button cmdExit;
        private System.Windows.Forms.Button cmdMinimize;
        public System.Windows.Forms.Label lblUser;
        public MetroFramework.Controls.MetroPanel pnlMain;
        private System.Windows.Forms.Button cmdBack;
    }
}