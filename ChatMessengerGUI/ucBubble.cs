﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Model.Database.Entity;
using Model.Database;
using Model;
using Message = Model.Database.Entity.Message;

namespace ChatMessengerGUI
{
    /// <summary>
    /// This user control is the design for each message 
    /// </summary>
    public partial class ucBubble : UserControl
    {
        #region private attributes
        private User user;
        private Contact contact;

        #endregion private attributes

        #region constructor

        /// <summary>
        /// This constructor is designed to create messages for the user and recieving messages
        /// </summary>
        public ucBubble()
        {
        }

        /// <summary>
        /// This constructor is designed to create messages for the user and recieving messages
        /// </summary>
        /// <param name="message"></param>
        /// <param name="time"></param>
        /// <param name="messagetype"></param>
        /// <param name="sender"></param>
        public ucBubble(User user,string message, string time, MsgType messagetype, string sender)
        {
            this.user = user;

            InitializeComponent();
            lblMessage.Text = sender + " " + message;
            lblTime.Text = time;
            

            if (messagetype.ToString() == "In")
            {
                //incoming message
                this.BackColor = Color.FromArgb(0, 164, 147);
            }
            else
            {
                //Outgoing Message
                this.BackColor = Color.DarkGray;
                lblTime.Left = lblTime.Left + 370;
            }
        }

        #endregion constructor

        #region private methods

        private void Setheight()
        { 
            Size maxSize = new Size(495, int.MaxValue);
            Graphics g = CreateGraphics();
            SizeF size = g.MeasureString(lblMessage.Text, lblMessage.Font, lblMessage.Width);
          
            lblMessage.Height = int.Parse(Math.Round(size.Height + 2, 0).ToString());
            lblTime.Top = lblMessage.Bottom + 10;
            this.Height = lblTime.Bottom + 10;
        }

        private void ucBubble_Resize(object sender, EventArgs e)
        {
            Setheight();
        }


        private void deleteMessageToolStripMenuItem_Click(object sender, EventArgs e)
        {
   
          
            Message message = this.Tag as Message;
            DBMessageManager messageManager = new DBMessageManager(Settings.Instance.DbConnectionString,message);
            foreach (Conversation conv in user.Conversations)
            {
                foreach (Message msge in conv.Messages)
                {

                    if (msge.Id == message.Id)
                    {
                        if (msge.SenderId == user.Id)
                        {

                            messageManager.DeleteMessage(conv.Messages);
                            break;
                        }
                        else
                        {
                            MessageBox.Show("You cannot delete this message, you are only allowed to delete your messages !", @"Error | Chat Messenger");
                        }

                    }  
                }

            }
        }
        #endregion private methods
    }

    #region public methods
    /// <summary>
    /// This public enum is used to check if it's an incoming message or out coming and make a difference between them
    /// </summary>
    public enum MsgType
    {
        /// <summary>
        /// Incoming message
        /// </summary>
        In,
        /// <summary>
        /// Out coming message
        /// </summary>
        Out
    }
    #endregion public methods
}
