﻿namespace ChatMessengerGUI
{
    partial class frmSign
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSign));
            this.pnlSign = new MetroFramework.Controls.MetroPanel();
            this.SuspendLayout();
            // 
            // pnlSign
            // 
            this.pnlSign.AutoSize = true;
            this.pnlSign.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlSign.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSign.HorizontalScrollbarBarColor = true;
            this.pnlSign.HorizontalScrollbarHighlightOnWheel = false;
            this.pnlSign.HorizontalScrollbarSize = 10;
            this.pnlSign.Location = new System.Drawing.Point(20, 60);
            this.pnlSign.Name = "pnlSign";
            this.pnlSign.Size = new System.Drawing.Size(775, 370);
            this.pnlSign.TabIndex = 0;
            this.pnlSign.VerticalScrollbarBarColor = true;
            this.pnlSign.VerticalScrollbarHighlightOnWheel = false;
            this.pnlSign.VerticalScrollbarSize = 10;
            // 
            // frmSign
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pnlSign);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmSign";
            this.Padding = new System.Windows.Forms.Padding(20, 60, 5, 20);
            this.Resizable = false;
            this.Text = "Chat Messenger";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSign_FormClosing_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroPanel pnlSign;
    }
}

