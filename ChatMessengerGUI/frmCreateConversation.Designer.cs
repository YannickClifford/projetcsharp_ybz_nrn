﻿namespace ChatMessengerGUI
{
    partial class frmCreateConversation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCreateConversation));
            this.cmdAdd = new System.Windows.Forms.Button();
            this.lstBox = new MetroFramework.Controls.MetroListView();
            this.colNickname = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.picBoxAddContact = new System.Windows.Forms.PictureBox();
            this.lblContact = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxAddContact)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdAdd
            // 
            this.cmdAdd.BackColor = System.Drawing.Color.LightSeaGreen;
            this.cmdAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAdd.FlatAppearance.BorderSize = 0;
            this.cmdAdd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed;
            this.cmdAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAdd.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAdd.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.cmdAdd.Location = new System.Drawing.Point(-1, 294);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(321, 55);
            this.cmdAdd.TabIndex = 14;
            this.cmdAdd.Text = "Create a conversation with this person";
            this.cmdAdd.UseVisualStyleBackColor = false;
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // lstBox
            // 
            this.lstBox.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colNickname});
            this.lstBox.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lstBox.FullRowSelect = true;
            this.lstBox.Location = new System.Drawing.Point(-1, 63);
            this.lstBox.MultiSelect = false;
            this.lstBox.Name = "lstBox";
            this.lstBox.OwnerDraw = true;
            this.lstBox.Size = new System.Drawing.Size(321, 190);
            this.lstBox.TabIndex = 15;
            this.lstBox.Tag = "Contact";
            this.lstBox.UseCompatibleStateImageBehavior = false;
            this.lstBox.UseSelectable = true;
            this.lstBox.View = System.Windows.Forms.View.Details;
            // 
            // colNickname
            // 
            this.colNickname.Text = "Contacts";
            this.colNickname.Width = 300;
            // 
            // picBoxAddContact
            // 
            this.picBoxAddContact.Image = global::ChatMessengerGUI.Properties.Resources.addContact;
            this.picBoxAddContact.Location = new System.Drawing.Point(2, 256);
            this.picBoxAddContact.Name = "picBoxAddContact";
            this.picBoxAddContact.Size = new System.Drawing.Size(32, 32);
            this.picBoxAddContact.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picBoxAddContact.TabIndex = 17;
            this.picBoxAddContact.TabStop = false;
            this.picBoxAddContact.Click += new System.EventHandler(this.picBoxAddContact_Click);
            this.picBoxAddContact.MouseEnter += new System.EventHandler(this.picBoxAddContact_MouseEnter);
            this.picBoxAddContact.MouseLeave += new System.EventHandler(this.picBoxAddContact_MouseLeave);
            // 
            // lblContact
            // 
            this.lblContact.Location = new System.Drawing.Point(40, 256);
            this.lblContact.Name = "lblContact";
            this.lblContact.Size = new System.Drawing.Size(85, 32);
            this.lblContact.TabIndex = 16;
            this.lblContact.Text = "Add Contact";
            this.lblContact.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmCreateConversation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(321, 372);
            this.Controls.Add(this.picBoxAddContact);
            this.Controls.Add(this.lblContact);
            this.Controls.Add(this.lstBox);
            this.Controls.Add(this.cmdAdd);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmCreateConversation";
            this.Resizable = false;
            this.Text = "Create a conversation";
            this.Load += new System.EventHandler(this.frmCreateConversation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picBoxAddContact)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button cmdAdd;
        private MetroFramework.Controls.MetroListView lstBox;
        private System.Windows.Forms.ColumnHeader colNickname;
        private System.Windows.Forms.PictureBox picBoxAddContact;
        private MetroFramework.Controls.MetroLabel lblContact;
    }
}