﻿using System;
using MetroFramework;
using MetroFramework.Components;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Net.Mail;
using Model.Database.Entity;
using System.Collections.Generic;
using Model.Database;
using Model;
using static Model.Database.DBConversationManager;

namespace ChatMessengerGUI
{
    /// <summary>
    /// This form is designed to be able for the user to create a new conversation with a list of his contacts
    /// </summary>
    public partial class frmCreateConversation : MetroForm
    {
        #region private attributes
        private User user;
        #endregion private attributes

        #region constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        public frmCreateConversation(User user)
        {
            this.user = user;
            InitializeComponent();
            InitializeTheme();
        }
        #endregion constructor
  
        #region private methods
        private void InitializeTheme()
        {
            // Initialize Metro Modern Ui
            MetroStyleManager metroStyleManager = new MetroStyleManager();
            metroStyleManager.Style = MetroColorStyle.Black;
            metroStyleManager.Theme = MetroThemeStyle.Light;
            StyleManager = metroStyleManager;
        }

        private void frmCreateConversation_Load(object sender, EventArgs e)
        {
            Reload();
        }
        private void Reload()
        {

            foreach (var contact in user.Contacts)
            {
                ListViewItem item = new ListViewItem(contact.Nickname);
                item.SubItems.Add(contact.Nickname);
                item.Tag = contact;
                lstBox.Items.Add(item);
            }

            lstBox.UpdateScrollbar();
            if (user.Contacts.Count <= 0)
            {
                picBoxAddContact.Visible = true;
                lblContact.Visible = true;
                lstBox.Visible = false;

                picBoxAddContact.Top = picBoxAddContact.Location.Y - 190;
                lblContact.Top = lblContact.Location.Y - 190;

                Size = new System.Drawing.Size(Size.Width, Size.Height - 250);
            }
            else
            {
                Size = new System.Drawing.Size(321, 372);
                picBoxAddContact.Visible = false;
                lblContact.Visible = false;
                lstBox.Visible = true;
            }
        }

        private void cmdAdd_Click(object sender, EventArgs e)
        {
            try
            {
                var selected = lstBox.SelectedItems;

                foreach (ListViewItem item in selected)
                {
                    DBUserManager userManager = new DBUserManager(Settings.Instance.DbConnectionString, user);

                    var s = item.SubItems; //Selected Item
                    var tag = item.Tag;

                    Contact contact = tag as Contact; // Selected Contact
                    Conversation newcConv = new Conversation(user);
                    newcConv.OtherParticipant = contact; //Add participant

                    DBConversationManager convManager = new DBConversationManager(Settings.Instance.DbConnectionString, newcConv);
                    convManager.CreateConversation(contact);
                }
                DialogResult = DialogResult.OK;
            }
            catch (ConversationAlreadyExistsException) {
                MessageBox.Show(@"A conversation has been already made with this contact. Please check your conversations.", @"Chat Messenger | Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void picBoxAddContact_Click(object sender, EventArgs e)
        {
            frmAddContact contacts = new frmAddContact(user);
            contacts.ShowDialog();
            if (contacts.DialogResult == DialogResult.OK)
            {
                Reload();
            }
        }
        private void picBoxAddContact_MouseEnter(object sender, EventArgs e)
        {
            picBoxAddContact.Image = Properties.Resources.AddContact_Hover;
        }

        private void picBoxAddContact_MouseLeave(object sender, EventArgs e)
        {
            picBoxAddContact.Image = Properties.Resources.addContact;
        }
        #endregion private methods


    }
}
