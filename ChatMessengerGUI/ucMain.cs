﻿using System;
using System.Windows.Forms;
using Model.Database;
using Model.Database.Entity;
using System.Drawing;
using Model;
using System.Collections.Generic;

namespace ChatMessengerGUI
{
    /// <summary>
    /// This user control is the view for the user's home page
    /// </summary>
    public partial class ucMain : UserControl
    {
        #region private attributes
        private User user;
        private Contact lastNotification;
        private ucConversation openedConv;
        #endregion private attributes

        #region constructor
        /// <summary>
        /// This is designed to initiaize the components of the user control and gets the information of the logged user
        /// </summary>
        /// <param name="user"></param>
        public ucMain(User user)
        {
            this.user = user;
            InitializeComponent();
            Timer timer = new Timer();
            timer.Interval = (10 * 1000); // 10 secs
            timer.Tick += new EventHandler(tmrUpdate_Tick);
            timer.Start();
            UpdateUser();
            lblEmail.Text = this.user.Email;
        }
        #endregion constructor

        #region private methods
        private void picBoxAddContact_Click_1(object sender, EventArgs e)
        {
            frmAddContact contacts = new frmAddContact(user);
            contacts.ShowDialog();
            UpdateUser();
        }

        private void picBoxAddContact_MouseEnter(object sender, EventArgs e)
        {
            picBoxAddContact.Image = Properties.Resources.AddContact_Hover;
        }

        private void picBoxAddContact_MouseLeave(object sender, EventArgs e)
        {
            picBoxAddContact.Image = Properties.Resources.addContact;
        }

        private void cmdCreateConversation_Click(object sender, EventArgs e)
        {
            frmCreateConversation contacts = new frmCreateConversation(user);
            contacts.ShowDialog();
            UpdateUser();
        }
        private void UpdateUser()
        {
            bool flag = false;
            DBUserManager userManager = new DBUserManager(Settings.Instance.DbConnectionString, user);
            
            var tempCurrentConversations = userManager.SelectUserConversations();
            foreach (Conversation conv in tempCurrentConversations)
            {
                DBConversationManager msg = new DBConversationManager(Settings.Instance.DbConnectionString, conv);
                conv.Messages = msg.SelectConversationMessages();
            }

            foreach (Conversation _conversation in tempCurrentConversations)
            {
                foreach (Conversation userConv in user.Conversations)
                {
                    if (_conversation.Id == userConv.Id) {

                        if (_conversation.Messages.Count > userConv.Messages.Count)
                        {
                            if (frmMain.Instance.lblUser.Visible == true)
                            {
                                if (frmMain.Instance.WindowState == FormWindowState.Minimized || frmMain.Instance.lblUser.Text != userConv.OtherParticipant.Nickname)
                                {
                                    notification.ShowBalloonTip(5000, "New Message", " from " + userConv.OtherParticipant.Nickname + " Click for more information", ToolTipIcon.Info);
                                    lastNotification = userConv.OtherParticipant;
                                    flag = true;
                                }
                            }
                            else {
                                notification.ShowBalloonTip(5000, "New Message", " from " + userConv.OtherParticipant.Nickname + " Click for more information", ToolTipIcon.Info);
                                lastNotification = userConv.OtherParticipant;
                                flag = true;
                            }

                        }
                        if(_conversation.Messages.Count < userConv.Messages.Count)
                        {
                            notification.ShowBalloonTip(5000, "Message has been deleted", " Conversation with " + userConv.OtherParticipant.Nickname + " Click for more information", ToolTipIcon.Info);
                            lastNotification = userConv.OtherParticipant;
                            flag = true;
                        }
                    }

                }
            }
            var tempContacts = userManager.SelectUserContacts();
            var tempConv = userManager.SelectUserConversations();

            
            if (tempContacts.Count != user.Contacts.Count || tempConv.Count != listBoxConversations.Items.Count || listBoxConversations.Items.Count == 0 || flag == true)
            {
                flag = false;
                listBoxConversations.Items.Clear();
                user.Contacts = userManager.SelectUserContacts();
                user.Conversations = userManager.SelectUserConversations();

                foreach (Conversation conv in user.Conversations)
                {
                    DBConversationManager msg = new DBConversationManager(Settings.Instance.DbConnectionString, conv);
                    conv.Messages = msg.SelectConversationMessages();
                }
                lblNumber.Text = user.Contacts.Count.ToString();

                foreach (var conv in user.Conversations)
                {
                    ListViewItem item = new ListViewItem(conv.OtherParticipant.Nickname);
                    item.SubItems.Add(conv.UpdateDate == DateTime.MinValue ? conv.CreationDate.ToString() : conv.UpdateDate.ToString());
                    item.Tag = conv.OtherParticipant;
                    listBoxConversations.Items.Add(item);

                    listBoxConversations.UpdateScrollbar();
                }
            }

        }

        private void ucMain_Load(object sender, EventArgs e)
        {
            UpdateUser();
        }

        private void listBoxConversations_DoubleClick(object sender, EventArgs e)
        {
            var selected = listBoxConversations.SelectedItems;
            
            foreach (ListViewItem item in selected)
            {

                var s = item.SubItems; //Selected Item
                var tag = item.Tag;
                Contact contact = tag as Contact; // Selected Contact
                frmMain.Instance.Content.Controls.Clear();
                frmMain.Instance.pnlMain.Controls.Clear();
                openedConv = new ucConversation(user, contact);
                frmMain.Instance.pnlMain.Controls.Add(openedConv);


            }
            frmMain.Instance.DisplayBackButton();
        }
        private void tmrUpdate_Tick(object sender, EventArgs e)
        {
            UpdateUser();
        }


        private void notification_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void notification_BalloonTipClicked(object sender, EventArgs e)
        {
            if (frmMain.Instance.WindowState == FormWindowState.Minimized)
            {
                frmMain.Instance.WindowState = FormWindowState.Normal;
            }
            frmMain.Instance.Content.Controls.Clear();
            frmMain.Instance.pnlMain.Controls.Add(new ucConversation(user, lastNotification));
            frmMain.Instance.DisplayBackButton();
        }

        private void deleteConversationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var selected = listBoxConversations.SelectedItems;

            foreach (ListViewItem item in selected)
            {

                var s = item.SubItems; //Selected Item
                var tag = item.Tag;
                Contact contact = tag as Contact; // Selected Contact
                foreach (Conversation conv in user.Conversations)
                {
                    if (conv.OtherParticipant.Id == contact.Id)
                    {
                        DBConversationManager convManager = new DBConversationManager(Settings.Instance.DbConnectionString, conv);
                        convManager.DeleteConversation(user.Conversations);
                        break;
 
                    }
                }

            }

            UpdateUser();
        }
        #endregion private methods
    }
}
