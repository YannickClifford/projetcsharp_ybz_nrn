﻿namespace ChatMessengerGUI
{
    partial class ucMain
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucMain));
            this.cmdCreateConversation = new System.Windows.Forms.Button();
            this.listBoxConversations = new MetroFramework.Controls.MetroListView();
            this.colContactConversation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colLastMessage = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteConversationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblNumber = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblConnected = new System.Windows.Forms.Label();
            this.lblContacts = new System.Windows.Forms.Label();
            this.tmrUpdate = new System.Windows.Forms.Timer(this.components);
            this.notification = new System.Windows.Forms.NotifyIcon(this.components);
            this.picBoxAddContact = new System.Windows.Forms.PictureBox();
            this.cmdProfile = new Bunifu.Framework.UI.BunifuTileButton();
            this.contextMenu.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxAddContact)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdCreateConversation
            // 
            this.cmdCreateConversation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(140)))), ((int)(((byte)(127)))));
            this.cmdCreateConversation.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdCreateConversation.FlatAppearance.BorderSize = 0;
            this.cmdCreateConversation.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed;
            this.cmdCreateConversation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCreateConversation.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdCreateConversation.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.cmdCreateConversation.Location = new System.Drawing.Point(563, 334);
            this.cmdCreateConversation.Name = "cmdCreateConversation";
            this.cmdCreateConversation.Size = new System.Drawing.Size(150, 32);
            this.cmdCreateConversation.TabIndex = 9;
            this.cmdCreateConversation.Text = "New conversation";
            this.cmdCreateConversation.UseVisualStyleBackColor = false;
            this.cmdCreateConversation.Click += new System.EventHandler(this.cmdCreateConversation_Click);
            // 
            // listBoxConversations
            // 
            this.listBoxConversations.BackColor = System.Drawing.Color.White;
            this.listBoxConversations.BackgroundImage = global::ChatMessengerGUI.Properties.Resources.whatsapp;
            this.listBoxConversations.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colContactConversation,
            this.colLastMessage});
            this.listBoxConversations.ContextMenuStrip = this.contextMenu;
            this.listBoxConversations.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.listBoxConversations.FullRowSelect = true;
            this.listBoxConversations.Location = new System.Drawing.Point(0, 3);
            this.listBoxConversations.MultiSelect = false;
            this.listBoxConversations.Name = "listBoxConversations";
            this.listBoxConversations.OwnerDraw = true;
            this.listBoxConversations.Size = new System.Drawing.Size(420, 364);
            this.listBoxConversations.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listBoxConversations.TabIndex = 10;
            this.listBoxConversations.UseCompatibleStateImageBehavior = false;
            this.listBoxConversations.UseSelectable = true;
            this.listBoxConversations.View = System.Windows.Forms.View.Details;
            this.listBoxConversations.DoubleClick += new System.EventHandler(this.listBoxConversations_DoubleClick);
            // 
            // colContactConversation
            // 
            this.colContactConversation.Text = "Conversations";
            this.colContactConversation.Width = 150;
            // 
            // colLastMessage
            // 
            this.colLastMessage.Text = "Last message";
            this.colLastMessage.Width = 265;
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteConversationToolStripMenuItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(181, 26);
            // 
            // deleteConversationToolStripMenuItem
            // 
            this.deleteConversationToolStripMenuItem.Name = "deleteConversationToolStripMenuItem";
            this.deleteConversationToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.deleteConversationToolStripMenuItem.Text = "Delete Conversation";
            this.deleteConversationToolStripMenuItem.Click += new System.EventHandler(this.deleteConversationToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(164)))), ((int)(((byte)(147)))));
            this.panel1.Controls.Add(this.lblNumber);
            this.panel1.Controls.Add(this.lblEmail);
            this.panel1.Controls.Add(this.lblConnected);
            this.panel1.Controls.Add(this.lblContacts);
            this.panel1.Location = new System.Drawing.Point(563, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(188, 321);
            this.panel1.TabIndex = 19;
            // 
            // lblNumber
            // 
            this.lblNumber.AutoSize = true;
            this.lblNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblNumber.ForeColor = System.Drawing.Color.White;
            this.lblNumber.Location = new System.Drawing.Point(166, 82);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(16, 18);
            this.lblNumber.TabIndex = 22;
            this.lblNumber.Text = "0";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblEmail.ForeColor = System.Drawing.Color.White;
            this.lblEmail.Location = new System.Drawing.Point(6, 55);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(171, 18);
            this.lblEmail.TabIndex = 21;
            this.lblEmail.Text = "nathan.rayburn@cpnv.ch";
            // 
            // lblConnected
            // 
            this.lblConnected.AutoSize = true;
            this.lblConnected.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblConnected.ForeColor = System.Drawing.Color.White;
            this.lblConnected.Location = new System.Drawing.Point(6, 31);
            this.lblConnected.Name = "lblConnected";
            this.lblConnected.Size = new System.Drawing.Size(108, 18);
            this.lblConnected.TabIndex = 20;
            this.lblConnected.Text = "Connected as :";
            // 
            // lblContacts
            // 
            this.lblContacts.AutoSize = true;
            this.lblContacts.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblContacts.ForeColor = System.Drawing.Color.White;
            this.lblContacts.Location = new System.Drawing.Point(6, 82);
            this.lblContacts.Name = "lblContacts";
            this.lblContacts.Size = new System.Drawing.Size(157, 18);
            this.lblContacts.TabIndex = 19;
            this.lblContacts.Text = "Number of contact(s) :";
            // 
            // tmrUpdate
            // 
            this.tmrUpdate.Tick += new System.EventHandler(this.tmrUpdate_Tick);
            // 
            // notification
            // 
            this.notification.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notification.Icon = ((System.Drawing.Icon)(resources.GetObject("notification.Icon")));
            this.notification.Visible = true;
            this.notification.BalloonTipClicked += new System.EventHandler(this.notification_BalloonTipClicked);
            this.notification.MouseClick += new System.Windows.Forms.MouseEventHandler(this.notification_MouseClick);
            // 
            // picBoxAddContact
            // 
            this.picBoxAddContact.BackColor = System.Drawing.Color.White;
            this.picBoxAddContact.BackgroundImage = global::ChatMessengerGUI.Properties.Resources.whatsapp;
            this.picBoxAddContact.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picBoxAddContact.Image = global::ChatMessengerGUI.Properties.Resources.addContact;
            this.picBoxAddContact.Location = new System.Drawing.Point(719, 335);
            this.picBoxAddContact.Name = "picBoxAddContact";
            this.picBoxAddContact.Size = new System.Drawing.Size(32, 32);
            this.picBoxAddContact.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picBoxAddContact.TabIndex = 8;
            this.picBoxAddContact.TabStop = false;
            this.picBoxAddContact.Click += new System.EventHandler(this.picBoxAddContact_Click_1);
            this.picBoxAddContact.MouseEnter += new System.EventHandler(this.picBoxAddContact_MouseEnter);
            this.picBoxAddContact.MouseLeave += new System.EventHandler(this.picBoxAddContact_MouseLeave);
            // 
            // cmdProfile
            // 
            this.cmdProfile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(140)))), ((int)(((byte)(127)))));
            this.cmdProfile.color = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(140)))), ((int)(((byte)(127)))));
            this.cmdProfile.colorActive = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(164)))), ((int)(((byte)(147)))));
            this.cmdProfile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdProfile.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.cmdProfile.ForeColor = System.Drawing.Color.White;
            this.cmdProfile.Image = global::ChatMessengerGUI.Properties.Resources.tor_msg_logo;
            this.cmdProfile.ImagePosition = 20;
            this.cmdProfile.ImageZoom = 80;
            this.cmdProfile.LabelPosition = 41;
            this.cmdProfile.LabelText = "My Profile";
            this.cmdProfile.Location = new System.Drawing.Point(426, 6);
            this.cmdProfile.Margin = new System.Windows.Forms.Padding(6);
            this.cmdProfile.Name = "cmdProfile";
            this.cmdProfile.Size = new System.Drawing.Size(128, 359);
            this.cmdProfile.TabIndex = 14;
            // 
            // ucMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ChatMessengerGUI.Properties.Resources.whatsapp;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.picBoxAddContact);
            this.Controls.Add(this.cmdProfile);
            this.Controls.Add(this.listBoxConversations);
            this.Controls.Add(this.cmdCreateConversation);
            this.Name = "ucMain";
            this.Size = new System.Drawing.Size(772, 384);
            this.Load += new System.EventHandler(this.ucMain_Load);
            this.contextMenu.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxAddContact)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picBoxAddContact;
        private System.Windows.Forms.Button cmdCreateConversation;
        private MetroFramework.Controls.MetroListView listBoxConversations;
        private System.Windows.Forms.ColumnHeader colContactConversation;
        private System.Windows.Forms.ColumnHeader colLastMessage;
        private Bunifu.Framework.UI.BunifuTileButton cmdProfile;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Timer tmrUpdate;
        private System.Windows.Forms.NotifyIcon notification;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem deleteConversationToolStripMenuItem;
        private System.Windows.Forms.Label lblNumber;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblConnected;
        private System.Windows.Forms.Label lblContacts;
    }
}
