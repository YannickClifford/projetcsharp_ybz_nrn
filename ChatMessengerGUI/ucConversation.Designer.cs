﻿namespace ChatMessengerGUI
{
    partial class ucConversation
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucConversation));
            this.txtBoxMessage = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bottom = new System.Windows.Forms.PictureBox();
            this.cmdRefresh = new Bunifu.Framework.UI.BunifuImageButton();
            this.cmdSend = new Bunifu.Framework.UI.BunifuImageButton();
            this.pnlMessage = new System.Windows.Forms.Panel();
            this.tmr = new System.Windows.Forms.Timer(this.components);
            this.notification = new System.Windows.Forms.NotifyIcon(this.components);
            this.ucBubble1 = new ChatMessengerGUI.ucBubble();
            ((System.ComponentModel.ISupportInitialize)(this.bottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSend)).BeginInit();
            this.pnlMessage.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtBoxMessage
            // 
            this.txtBoxMessage.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtBoxMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.txtBoxMessage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtBoxMessage.HintForeColor = System.Drawing.Color.Empty;
            this.txtBoxMessage.HintText = "";
            this.txtBoxMessage.isPassword = false;
            this.txtBoxMessage.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(180)))), ((int)(((byte)(120)))));
            this.txtBoxMessage.LineIdleColor = System.Drawing.Color.DarkGray;
            this.txtBoxMessage.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(164)))), ((int)(((byte)(147)))));
            this.txtBoxMessage.LineThickness = 3;
            this.txtBoxMessage.Location = new System.Drawing.Point(14, 515);
            this.txtBoxMessage.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.txtBoxMessage.Name = "txtBoxMessage";
            this.txtBoxMessage.Size = new System.Drawing.Size(1016, 51);
            this.txtBoxMessage.TabIndex = 2;
            this.txtBoxMessage.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtBoxMessage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBoxMessage_KeyPress);
            // 
            // bottom
            // 
            this.bottom.Location = new System.Drawing.Point(25, 142);
            this.bottom.Name = "bottom";
            this.bottom.Size = new System.Drawing.Size(100, 50);
            this.bottom.TabIndex = 1;
            this.bottom.TabStop = false;
            this.bottom.Visible = false;
            // 
            // cmdRefresh
            // 
            this.cmdRefresh.BackColor = System.Drawing.Color.Transparent;
            this.cmdRefresh.Image = global::ChatMessengerGUI.Properties.Resources.refresh_512;
            this.cmdRefresh.ImageActive = null;
            this.cmdRefresh.Location = new System.Drawing.Point(1040, 515);
            this.cmdRefresh.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmdRefresh.Name = "cmdRefresh";
            this.cmdRefresh.Size = new System.Drawing.Size(60, 51);
            this.cmdRefresh.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.cmdRefresh.TabIndex = 3;
            this.cmdRefresh.TabStop = false;
            this.cmdRefresh.Zoom = 10;
            this.cmdRefresh.Click += new System.EventHandler(this.cmdRefresh_Click);
            // 
            // cmdSend
            // 
            this.cmdSend.BackColor = System.Drawing.Color.Transparent;
            this.cmdSend.Image = global::ChatMessengerGUI.Properties.Resources.send_128;
            this.cmdSend.ImageActive = null;
            this.cmdSend.Location = new System.Drawing.Point(1095, 515);
            this.cmdSend.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Size = new System.Drawing.Size(56, 51);
            this.cmdSend.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.cmdSend.TabIndex = 1;
            this.cmdSend.TabStop = false;
            this.cmdSend.Zoom = 10;
            this.cmdSend.Click += new System.EventHandler(this.cmdSend_Click);
            // 
            // pnlMessage
            // 
            this.pnlMessage.AutoScroll = true;
            this.pnlMessage.BackColor = System.Drawing.Color.Transparent;
            this.pnlMessage.Controls.Add(this.ucBubble1);
            this.pnlMessage.Location = new System.Drawing.Point(14, 5);
            this.pnlMessage.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pnlMessage.Name = "pnlMessage";
            this.pnlMessage.Size = new System.Drawing.Size(1137, 502);
            this.pnlMessage.TabIndex = 0;
            // 
            // tmr
            // 
            this.tmr.Tick += new System.EventHandler(this.tmr_Tick);
            // 
            // notification
            // 
            this.notification.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notification.Icon = ((System.Drawing.Icon)(resources.GetObject("notification.Icon")));
            this.notification.Visible = true;
            // 
            // ucBubble1
            // 
            this.ucBubble1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(164)))), ((int)(((byte)(147)))));
            this.ucBubble1.Enabled = false;
            this.ucBubble1.Location = new System.Drawing.Point(0, 5);
            this.ucBubble1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ucBubble1.Name = "ucBubble1";
            this.ucBubble1.Size = new System.Drawing.Size(772, 155);
            this.ucBubble1.TabIndex = 1;
            this.ucBubble1.Visible = false;
            // 
            // ucConversation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ChatMessengerGUI.Properties.Resources.whatsapp;
            this.Controls.Add(this.cmdRefresh);
            this.Controls.Add(this.txtBoxMessage);
            this.Controls.Add(this.cmdSend);
            this.Controls.Add(this.pnlMessage);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "ucConversation";
            this.Size = new System.Drawing.Size(1155, 615);
            ((System.ComponentModel.ISupportInitialize)(this.bottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRefresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSend)).EndInit();
            this.pnlMessage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Bunifu.Framework.UI.BunifuImageButton cmdSend;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtBoxMessage;
        private Bunifu.Framework.UI.BunifuImageButton cmdRefresh;
        private System.Windows.Forms.PictureBox bottom;
        private System.Windows.Forms.Panel pnlMessage;
        private System.Windows.Forms.Timer tmr;
        private System.Windows.Forms.NotifyIcon notification;
        private ucBubble ucBubble1;
    }
}
