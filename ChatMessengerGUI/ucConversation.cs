﻿using System;
using System.Windows.Forms;
using Model.Database;
using Model.Database.Entity;
using Model;
using System.Drawing;
using Message = Model.Database.Entity.Message;

namespace ChatMessengerGUI
{
    /// <summary>
    /// This form is designed to have a conversation with the user's contacts
    /// </summary>
    public partial class ucConversation : UserControl
    {
        #region private attributes
        private User user;
        private Contact participant;
        private Contact lastNotification;

        #endregion private attributes

        #region attributes
        ucBubble bubbleOld = new ucBubble();
        #endregion attributes

        #region constructor

        /// <summary>
        /// This constructor will initialize all of the components of the user control
        /// </summary>
        /// <param name="user"></param>
        /// <param name="contact"></param>
        public ucConversation(User user,Contact contact)
        {
            this.participant = contact;
            this.user = user;
            InitializeComponent();
            
            Timer timer = new Timer();
            timer.Interval = (1000); // 1 secs
            timer.Tick += new EventHandler(tmr_Tick);
            timer.Start();

            var name = contact.Nickname;
            char.ToUpper(name[0]);
            frmMain.Instance.ChangeNickName_label(name);

            ucBubble1.Visible = false;
            bubbleOld.Top = 0 - bubbleOld.Height + 10;
            txtBoxMessage.Visible = true;
            cmdRefresh.Visible = true;
            cmdSend.Visible = true;
            pnlMessage.Controls.Clear();
            RefreshConversation();
        }
        #endregion constructor

        #region private methods
        private void txtBoxMessage_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                cmdSend_Click(sender, e);
            }
        }
        private void addInMessage(Message message, string time, string sender)
        {
            ucBubble bubble = new ucBubble(this.user,message.Content, time, MsgType.In, sender + " :");
            bubble.Location = ucBubble1.Location;
            bubble.Size = ucBubble1.Size;
            bubble.Anchor = ucBubble1.Anchor;
            bubble.Top = bubbleOld.Bottom + 10;
            bubble.Tag = message;
            pnlMessage.Controls.Add(bubble);
            
            pnlMessage.VerticalScroll.Value = pnlMessage.VerticalScroll.Maximum;

            bubbleOld = bubble;
        }
        private void addOutMessage(Message message, string time,string sender)
        {
            ucBubble bubble = new ucBubble(this.user,message.Content, time, MsgType.Out, "Me :");
            bubble.Location = ucBubble1.Location;
            bubble.Size = ucBubble1.Size;
            bubble.Anchor = ucBubble1.Anchor;
            bubble.Top = bubbleOld.Bottom + 10;
            bubble.Left = ucBubble1.Left + 200;
            bubble.Tag = message;
            pnlMessage.Controls.Add(bubble);

            bottom.Top = bubble.Bottom + 50;

            bubbleOld = bubble;
        }


        private void cmdSend_Click(object sender, EventArgs e)
        {

            if (txtBoxMessage.Text == "")
            {
                txtBoxMessage.LineFocusedColor = Color.LightCoral;
                Shake(frmMain.Instance);
            }
            else {
                Conversation thisConv = null; //gets the actual conversation

                foreach(Conversation conv in user.Conversations)
                {
                    if (conv.OtherParticipant.Nickname == participant.Nickname)
                    {
                        thisConv = conv;
                    }
                }
                
                Message newMsg = new Message(txtBoxMessage.Text,user.Id); // New message
                DBConversationManager convManager = new DBConversationManager(Settings.Instance.DbConnectionString,thisConv);
                DBMessageManager messageManager = new DBMessageManager(Settings.Instance.DbConnectionString,newMsg);
                messageManager.AddMessage(thisConv,convManager);

                txtBoxMessage.LineFocusedColor = Color.FromArgb(2, 180, 120);
                addOutMessage(newMsg, DateTime.Now.ToString("MM/dd/yyyy hh:mm tt"), "Me :");
                pnlMessage.VerticalScroll.Value = pnlMessage.VerticalScroll.Maximum; //scroll the chatbox down 
                txtBoxMessage.Text = "";
            }
        }
        private static void Shake(Form form)
        {
            var original = form.Location;
            var rnd = new Random(1337);
            const int shake_amplitude = 10;
            for (int i = 0; i < 10; i++)
            {
                form.Location = new Point(original.X + rnd.Next(-shake_amplitude, shake_amplitude), original.Y + rnd.Next(-shake_amplitude, shake_amplitude));
                System.Threading.Thread.Sleep(20);
            }
            form.Location = original;
        }

        private void cmdRefresh_Click(object sender, EventArgs e)
        {
           RefreshConversation();
        }

        private void tmr_Tick(object sender, EventArgs e)
        {
            RefreshConversation();
        }
        #endregion private methods

        #region public methods
        /// <summary>
        /// This function will refresh the messages in the conversation
        /// </summary>
        private void RefreshConversation()
        {
            bool flag = false;
            foreach (Conversation conv in user.Conversations)
            {
                if (conv.OtherParticipant.Nickname == participant.Nickname)
                {
                    flag = true;
                    DBConversationManager convManager = new DBConversationManager(Settings.Instance.DbConnectionString, conv);
                    var messages = convManager.SelectConversationMessages();
                    if (messages.Count != pnlMessage.Controls.Count)
                    {
                        pnlMessage.Controls.Clear();
                        foreach (Message msg in messages)
                        {
                            if (msg.SenderId == user.Id)
                            {
                                addOutMessage(msg, msg.CreationDate.ToString(), "Me :");
                            }
                            else
                            {
                                addInMessage(msg, msg.CreationDate.ToString(), participant.Nickname);
                            }

                        }
                        pnlMessage.VerticalScroll.Value = pnlMessage.VerticalScroll.Maximum; //scroll the chatbox down 
                    }

                }
            }
            if (flag == false)
            {
                flag = true;
                txtBoxMessage.Visible = false;
                cmdRefresh.Visible = false;
                cmdSend.Visible = false;
                
                ucBubble bubble = new ucBubble(this.user, "This conversation has been deleted", DateTime.Now.ToString(), MsgType.Out, "Chat Messenger :");
                bubble.Location = ucBubble1.Location;
                bubble.Size = ucBubble1.Size;
                bubble.Anchor = ucBubble1.Anchor;
                bubble.Top = bubbleOld.Bottom + 10;
                bubble.Left = ucBubble1.Left + 200;
                pnlMessage.Controls.Add(bubble);

                bottom.Top = bubble.Bottom + 50;

                bubbleOld = bubble;
                pnlMessage.VerticalScroll.Value = pnlMessage.VerticalScroll.Maximum; //scroll the chatbox down 
            }

        }
        #endregion public methods
    }
}
