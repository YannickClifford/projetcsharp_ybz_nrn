﻿using System;
using System.Windows.Forms;
using System.Drawing;
using MetroFramework;
using MetroFramework.Components;
using MetroFramework.Controls;
using MetroFramework.Forms;
using Model;
using System.IO;
using Model.Database.Entity;
using MaterialSkin.Controls;
using MaterialSkin;

namespace ChatMessengerGUI
{
    /// <summary>
    /// This form is designed for the home page and inherited from an external library theme for windows form
    /// </summary>
    public partial class frmMain : MetroForm
    {
        #region private attributes
        private static frmMain instance;
        private User user;
        Point PanelMouseDownLocation;
        #endregion private attributes

        #region constructor

        /// <summary>
        /// This constructor :
        /// - Creates an instance of the home page form
        /// - Initializes the components of the form
        /// - Loads the theme for windows form from an external library
        /// </summary>
        public frmMain()
        {
            instance = this;
            InitializeComponent();
            InitializeTheme();

        }
        /// <summary>
        /// This constructor is designed to initialize the components of form with the metro theme
        /// </summary>
        /// <param name="user"></param>
        public frmMain(User user)
        {
            this.user = user;
            instance = this;
            InitializeComponent();
            InitializeTheme();
            pnlMain.Controls.Add(new ucMain(user));
        }
        #endregion constructor

        #region accessor
        /// <summary>
        /// This accessor is designed to be the only instance of frmMain
        /// </summary>
        public static frmMain Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new frmMain();
                }

                return instance;
            }
        }

        /// <summary>
        /// Computer generated from the Metro library
        /// Use the panel for the user controls such as login and register
        /// </summary>
        public MetroPanel Content
        {
            get => pnlMain;
            set => pnlMain = value;
        }

        #endregion accessor

        #region private methods
        private void InitializeTheme()
        {
            // Initialize Metro Modern Ui
            MetroStyleManager metroStyleManager = new MetroStyleManager();
            metroStyleManager.Style = MetroColorStyle.Black;
            metroStyleManager.Theme = MetroThemeStyle.Light;
            StyleManager = metroStyleManager;

        }
        /// <summary>
        /// This function is designed to ask the user if he wants to close the application and also save the settings of the application to the json file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                dynamic result = MessageBox.Show("Voulez-vous quitter ?", "Chat Messeneger", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    Settings.Instance.WindowsX = Location.X;
                    Settings.Instance.WindowsY = Location.Y;
                    File.WriteAllText("settings.json", Settings.Instance.ToJson());
                    Application.Exit();
                }

                if (result == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }
        private void cmdLogout_Click(object sender, EventArgs e)
        {
            Settings.Instance.WindowsX = Location.X;
            Settings.Instance.WindowsY = Location.Y;
            File.WriteAllText("settings.json", Settings.Instance.ToJson());
            Application.Restart();
        }


        private void cmdMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void cmdExit_Click(object sender, EventArgs e)
        {
            dynamic result = MessageBox.Show("Voulez-vous quitter ?", "Chat Messeneger", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                Settings.Instance.WindowsX = Location.X;
                Settings.Instance.WindowsY = Location.Y;
                File.WriteAllText("settings.json", Settings.Instance.ToJson());
                Application.Exit();
            }
        }
        private void pnlDecorationHeader1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left) PanelMouseDownLocation = e.Location;
        }

        private void pnlDecorationHeader1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)

            {

                this.Left += e.X - PanelMouseDownLocation.X;

                this.Top += e.Y - PanelMouseDownLocation.Y;

            }
        }

        private void pnlDecorationHeader2_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left) PanelMouseDownLocation = e.Location;
        }

        private void pnlDecorationHeader2_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)

            {
                this.Left += e.X - PanelMouseDownLocation.X;

                this.Top += e.Y - PanelMouseDownLocation.Y;
            }
        }
        /// <summary>
        /// This function is designed leave the conversation back to the main menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdBack_Click_1(object sender, EventArgs e)
        {
            HideBackButton();
            lblUser.Visible = false;
            lblUser.Text = "";
            foreach (IDisposable control in pnlMain.Controls)
            {
                control.Dispose();
            }
            pnlMain.Controls.Clear();
            pnlMain.Controls.Add(new ucMain(user));
        }
        #endregion private methods

        #region public methods

        /// <summary>
        /// This function is designed to display the back button when the user is leaving the home page
        /// </summary>
        public void DisplayBackButton()
        {
            this.cmdBack.Visible = true;
        }
        /// <summary>
        /// This function is designed to hide the back buttom when the user is on the home page
        /// </summary>
        public void HideBackButton()
        {
            this.cmdBack.Visible = false;
        }

        /// <summary>
        /// This function is designed to show the user's contact's nickname in the header of the main form
        /// </summary>
        /// <param name="name"></param>
        public void ChangeNickName_label(string name)
        {
            lblUser.Visible = true;
            lblUser.Text = name;
        }



        #endregion public methods

    }
}
