﻿using System;
using MetroFramework;
using MetroFramework.Components;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Net.Mail;
using Model.Database.Entity;
using Model.Database;
using Model;

namespace ChatMessengerGUI
{
    /// <summary>
    /// This form will be used so the user can add a new contact to his list of contacts and be able to choose a nickname for the contact.
    /// </summary>
    public partial class frmAddContact : MetroForm
    {

        #region private attributes
        private User user;
        #endregion private attributes

        #region constructor
        /// <summary>
        /// This is designed to initialize the components of the user control and intialize the metro theme
        /// This also gets the details of the logged user
        /// </summary>
        /// <param name="user"></param>
        public frmAddContact(User user)
        {
            this.user = user;
            InitializeComponent();
            InitializeTheme();
        }
        #endregion constructor

        #region private methods
        /// <summary>
        /// This function is designed to test if the email is valid or not
        /// </summary>
        /// <param name="emailaddress"></param>
        /// <returns></returns>
        private bool IsValid(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        private void InitializeTheme()
        {
            // Initialize Metro Modern Ui
            MetroStyleManager metroStyleManager = new MetroStyleManager();
            metroStyleManager.Style = MetroColorStyle.Black;
            metroStyleManager.Theme = MetroThemeStyle.Light;
            StyleManager = metroStyleManager;
        }

        private void cmdAdd_Click_1(object sender, EventArgs e)
        {
            bool emailNotVaild = IsValid(txtEmail.Text);
            bool noNickname = string.IsNullOrWhiteSpace(txtNickname.Text);

            if (noNickname || !emailNotVaild)
            {
                MessageBox.Show(@"You need to fill up all of the fields !", @"Error in the form", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                if (emailNotVaild) txtEmail.WithError = true;

                if (noNickname) txtNickname.WithError = true;
                return;
            }
            if (user.Email == txtEmail.Text)
            {
                MessageBox.Show(@"You cannot add yourself into your contacts !", @"Error in the form", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtEmail.WithError = true;
                return;
            }

            //Try to add a contact to the user's contact list in the database
            try
            {
                Contact newContact = new Contact(txtEmail.Text,txtNickname.Text, user.Id);
                DBContactManager contactManager  = new DBContactManager(Settings.Instance.DbConnectionString,newContact);
                contactManager.AddContact(user);
                MessageBox.Show(@"This user has been added to your contacts with success !", @" Chat Messenger | Add Contact", MessageBoxButtons.OK);
                DialogResult = DialogResult.OK;
            }
            catch(Exception)
            {
                MessageBox.Show(@"User not found or is already in your contacts", @"Chat Messenger | Add Contact Error", MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }
        }
        #endregion private attributes
    }
}
